unit uFrmPesqProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Buttons,
  Data.DB, Vcl.Grids, Vcl.DBGrids;

type
  TFrmPesqProduto = class(TForm)
    DBGrid1: TDBGrid;
    dsPesqProduto: TDataSource;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    btnPesquisaProduto: TBitBtn;
    edtPesquisaProduto: TLabeledEdit;
    cbxTpPesquisa: TComboBox;
    Panel1: TPanel;
    btnConfirmar: TBitBtn;
    btnCancelar: TBitBtn;
    procedure edtPesquisaProdutoKeyPress(Sender: TObject; var Key: Char);
    procedure btnPesquisaProdutoClick(Sender: TObject);
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmPesqProduto: TFrmPesqProduto;

implementation

{$R *.dfm}

uses uDM;

procedure TFrmPesqProduto.btnCancelarClick(Sender: TObject);
begin
 dm.cdsConsultaProduto.Close;
 Close;
end;

procedure TFrmPesqProduto.btnConfirmarClick(Sender: TObject);
begin
 ModalResult := mrOk;
end;

procedure TFrmPesqProduto.btnPesquisaProdutoClick(Sender: TObject);
var
 S: TStringList;
begin
 S:= TStringList.Create;
 S.Add('SELECT p.produtoid,p.referencia, p.descricao, p.estoque, p.preco,');
 S.Add('       f.descricao , g.descricao');
 S.Add('from  produtos p, fabricante f, grupo g');
 S.Add('where p.codgrupo = g.cod');
 S.Add('  and p.codfabricante = f.cod');

 if cbxTpPesquisa.Text = 'REFERENCIA' then
 begin
   S.Add(' and p.referencia = '+ QuotedStr(edtPesquisaProduto.Text));
 end;

 if cbxTpPesquisa.Text = 'DESCRICAO' then
 begin
   S.Add(' and p.descricao like '+ QuotedStr(edtPesquisaProduto.Text+'%') )
 end;

 dm.cdsConsultaProduto.Close;
 dm.cdsConsultaProduto.CommandText := S.Text;
 dm.cdsConsultaProduto.Open;
end;

procedure TFrmPesqProduto.DBGrid1DblClick(Sender: TObject);
begin
 ModalResult := mrOk;
end;

procedure TFrmPesqProduto.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
 if key = #13 then
 begin
  ModalResult := mrOk;
 end;
end;

procedure TFrmPesqProduto.edtPesquisaProdutoKeyPress(Sender: TObject; var Key: Char);
begin
 if key = #13 then
  btnPesquisaProduto.SetFocus;
end;

end.
