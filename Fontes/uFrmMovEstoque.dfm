inherited FrmMovEstoque: TFrmMovEstoque
  Caption = 'Movimenta'#231#227'o de Estoque'
  ExplicitWidth = 1100
  ExplicitHeight = 661
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl1: TPageControl
    ActivePage = tbCadastro
    inherited tbCadastro: TTabSheet
      Caption = ''
      ExplicitTop = 6
      ExplicitWidth = 1076
      ExplicitHeight = 559
      object Label4: TLabel
        Left = 3
        Top = 3
        Width = 128
        Height = 13
        Caption = 'Tipo de Movimenta'#231#227'o'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 191
        Top = 3
        Width = 27
        Height = 13
        Caption = 'Data'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBGrid2: TDBGrid
        Left = 0
        Top = 208
        Width = 1076
        Height = 351
        Align = alBottom
        DataSource = DataSource1
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'REFERENCIA'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRICAO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QTDE'
            Visible = True
          end>
      end
      object cbxTpMovimentacao: TComboBox
        Left = 3
        Top = 22
        Width = 182
        Height = 21
        ItemIndex = 0
        TabOrder = 1
        Text = '<SELECIONE>'
        Items.Strings = (
          '<SELECIONE>'
          '1 - ENTRADA DE ESTOQUE'
          '2 - SAIDA DE ESTOQUE')
      end
      object GroupBox1: TGroupBox
        Left = 3
        Top = 56
        Width = 1070
        Height = 129
        Caption = 'Pesquisa Produtos'
        TabOrder = 2
        object Label6: TLabel
          Left = 9
          Top = 14
          Width = 80
          Height = 13
          Caption = 'Tipo de Pesquisa'
        end
        object Label7: TLabel
          Left = 9
          Top = 64
          Width = 19
          Height = 13
          Caption = 'Cod'
        end
        object Label8: TLabel
          Left = 828
          Top = 64
          Width = 27
          Height = 13
          Caption = 'QTDE'
        end
        object Label9: TLabel
          Left = 134
          Top = 64
          Width = 52
          Height = 13
          Caption = 'Referencia'
        end
        object btnPesquisaProduto: TBitBtn
          Left = 968
          Top = 28
          Width = 99
          Height = 25
          Action = ac_PesquisaProduto
          Caption = 'Pesquisar'
          Glyph.Data = {
            F6060000424DF606000000000000360000002800000018000000180000000100
            180000000000C0060000430B0000430B00000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF646873183D
            60FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            646873467BCB4D5A91183D60FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FF5192C64CA5EF467BCB4D5A91183D60FF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FF3C98E498D8F84BA9F5467BCB4D5A9118
            3D60FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF3C98E498D8
            F84AA5F2467BCB4D5A91183D60FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FF3C98E498D8F84BA9F5467BCB4D5A916D6676FF00FFC1AFAEBDA1
            9BC0A99EA992969FABB7FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FF3C98E498D8F851A7EF768BAA9D8481
            CFB0A5F0DEC6FAF1CAFEFBCDEEE0BFDDC4ADA49291FF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF3C98E46D
            6676AE958FD1BCADF8F0D2FFEDBEFFEBBBFFF1D1FFFAE2FFFDE1E9DBC6AA9A9A
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FF9FB8C4CCA89DFBF5D7FFE6B7FFE0AFFFEECFFFF2DEFFF4E5FF
            FDFDFFFFF9D5C1B2BEB9BCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFAE9A9BE8D5C0FFF4CBFFDAA3FFE5BCFFF0
            D9FFF0D5FFF5E8FFFEFEFFFEFBF2EBCED8C0B2FF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB99D8FFBF7DCFFEBBC
            FFD79FFFDFB2FFEFD8FFEDD6FFF0D9FFF4E2FFF3DFFCF0C3B99D8FFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB9
            9D8FFDFAE1FFEABCFFD79FFFD8A3FFDFB5FFEFDAFFEED7FFF1DAFFE4B7FEEFBD
            B99D8FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFA28E90ECDAC3FFFADBFFF2E1FFEFD9FFDBA7FFDDACFFE8C4FF
            E0AFFFE0A8FDF0CBB5ABA4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFDAB5A7FBF9EAFFFFFFFFF8F0FFDE
            B2FFD69CFFD89EFFDDA5FEF0C5E4D3BC969EA7FF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFD0B8B5E3CDBE
            FCF9EEFFFAE3FFE9BAFFE2B1FFE9BBFFF3CFE7DDC3C0A5A0FF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFD6BDB6DBBFAFEFE1C7FBF7D8FFF9DDF2E9CDCFB7A6C4A89FFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB79D98B99D8FB99D8FB68E89C3
            B7BAFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
          TabOrder = 0
        end
        object edtPesquisaProduto: TLabeledEdit
          Left = 160
          Top = 30
          Width = 802
          Height = 21
          CharCase = ecUpperCase
          EditLabel.Width = 69
          EditLabel.Height = 13
          EditLabel.Caption = 'Pesquisar Por:'
          TabOrder = 1
        end
        object cbxTpPesquisa: TComboBox
          Left = 9
          Top = 30
          Width = 145
          Height = 21
          ItemIndex = 0
          TabOrder = 2
          Text = 'REFERENCIA'
          Items.Strings = (
            'REFERENCIA'
            'DESCRICAO')
        end
        object edtDescricaoProduto: TLabeledEdit
          Left = 239
          Top = 80
          Width = 586
          Height = 21
          CharCase = ecUpperCase
          EditLabel.Width = 46
          EditLabel.Height = 13
          EditLabel.Caption = 'Descri'#231#227'o'
          ReadOnly = True
          TabOrder = 3
        end
        object btnAdicionar: TBitBtn
          Left = 968
          Top = 78
          Width = 99
          Height = 25
          Caption = 'Adicionar'
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FF006600006600FF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0066001EB2311FB13300
            6600FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FF00660031C24F22B7381AB02D21B437006600FF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FF00660047D36D3BCB5E25A83B0066001B
            A92E1DB132006600FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF006600
            4FD67953DE7F31B54D006600FF00FF006600179D271EAE31006600FF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FF00660041C563006600FF00FFFF00FFFF
            00FFFF00FF00660019AA2B006600FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FF006600FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF006600149D210066
            00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FF006600006600FF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FF006600006600FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
          TabOrder = 4
          OnClick = btnAdicionarClick
        end
        object edtCodProduto: TEdit
          Left = 9
          Top = 80
          Width = 119
          Height = 21
          NumbersOnly = True
          TabOrder = 5
        end
        object edtQTDE: TEdit
          Left = 828
          Top = 80
          Width = 134
          Height = 21
          NumbersOnly = True
          TabOrder = 6
        end
        object edtReferencia: TEdit
          Left = 134
          Top = 80
          Width = 99
          Height = 21
          NumbersOnly = True
          ReadOnly = True
          TabOrder = 7
        end
      end
      object dtInclusao: TDateTimePicker
        Left = 191
        Top = 22
        Width = 122
        Height = 21
        Date = 41930.460464930550000000
        Time = 41930.460464930550000000
        TabOrder = 3
      end
    end
  end
  inherited ActionList1: TActionList
    object ac_PesquisaProduto: TAction
      Caption = 'Pesquisar'
      OnExecute = ac_PesquisaProdutoExecute
    end
  end
  inherited Ds: TDataSource
    DataSet = DM.cdsMov_Estoque
    Left = 936
    Top = 48
  end
  object DsDetail: TDataSource
    DataSet = DM.cdsMovEstoqueItens
    Left = 976
    Top = 48
  end
  object DataSource1: TDataSource
    DataSet = cdsMovProdutos
    Left = 536
    Top = 320
  end
  object cdsMovProdutos: TClientDataSet
    Aggregates = <>
    AggregatesActive = True
    Params = <>
    Left = 1032
    Top = 416
    object cdsMovProdutosREFERENCIA: TStringField
      FieldName = 'REFERENCIA'
      Size = 10
    end
    object cdsMovProdutosDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 100
    end
    object cdsMovProdutosCODPROD: TIntegerField
      FieldName = 'CODPROD'
    end
    object cdsMovProdutosQTDE: TIntegerField
      FieldName = 'QTDE'
    end
    object cdsMovProdutosTOTALQTDE: TAggregateField
      FieldName = 'TOTALQTDE'
      Active = True
      DisplayName = ''
      Expression = 'SUM(QTDE)'
    end
  end
end
