unit uFrmAcesso;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModelo, Data.DB, Vcl.ImgList,
  Vcl.ActnList, Vcl.Menus, Vcl.ComCtrls, Vcl.ToolWin, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, Vcl.DBCtrls;

type
  TFrmAcesso = class(TFrmModelo)
    Label4: TLabel;
    Label5: TLabel;
    DBComboBox1: TDBComboBox;
    DBLookupComboBox1: TDBLookupComboBox;
    cbIncluir: TDBCheckBox;
    cbExcluir: TDBCheckBox;
    cbImprimir: TDBCheckBox;
    cbConsultar: TDBCheckBox;
    cbAlterar: TDBCheckBox;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmAcesso: TFrmAcesso;

implementation

{$R *.dfm}

uses uDM, Dbxcommon;

procedure TFrmAcesso.FormShow(Sender: TObject);
var
  DBCon:   TDBXConnection;
  Command: TDBXCommand;
  Reader:  TDBXReader;

begin
  inherited;

  DBCon := TDBXConnectionFactory.GetConnectionFactory.GetConnection('FBConnection','sysdba','masterkey');
  Command := DBCon.CreateCommand;
  Command.Text := 'Select DISTINCT FORMULARIO FROM ACESSOS ';
  Reader := Command.ExecuteQuery;
  DBComboBox1.Items.Clear;
  WHILE Reader.Next do
  begin
    DBComboBox1.Items.Add(Reader.Value[0].GetAnsiString)
  end;
end;

end.



