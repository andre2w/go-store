unit uFrmPesqConsignado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.Buttons, Vcl.ExtCtrls, Vcl.DBCtrls, Data.DB,
  Datasnap.DBClient, System.Actions, Vcl.ActnList, Datasnap.Provider,
  Data.FMTBcd, Data.SqlExpr, Vcl.Menus;

type
  TFrmPesqConsignado = class(TForm)
    PageControl1: TPageControl;
    tbConsulta: TTabSheet;
    tbCadastro: TTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    dtVendaI: TDateTimePicker;
    dtVendaF: TDateTimePicker;
    btnBuscar: TBitBtn;
    dbgConsignado: TDBGrid;
    Panel4: TPanel;
    btnVisualizar: TBitBtn;
    btnFechar: TBitBtn;
    btnIncluir: TBitBtn;
    GridProdutos: TDBGrid;
    Panel1: TPanel;
    BitBtn2: TBitBtn;
    BitBtn4: TBitBtn;
    GroupBox3: TGroupBox;
    edtPesqProdutos: TLabeledEdit;
    gridPesqProduto: TDBGrid;
    Label2: TLabel;
    cbxConsultaProduto: TComboBox;
    Label3: TLabel;
    cdsConsignado: TClientDataSet;
    Label4: TLabel;
    Label5: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    cdsConsignadoREFERENCIA: TStringField;
    cdsConsignadoCODPRODUTO: TIntegerField;
    cdsConsignadoDESCRICAO: TStringField;
    cdsConsignadoPRECO: TFloatField;
    cdsConsignadoVLTOTAL: TFloatField;
    cdsConsignadoQTDETOTAL: TAggregateField;
    cdsConsignadoSUBTOTAL: TAggregateField;
    dsConsignado: TDataSource;
    edtPessoa: TLabeledEdit;
    edtCodPessoa: TLabeledEdit;
    dsConsultaProdutos: TDataSource;
    cdsConsignadoQTDE: TFloatField;
    AcConsignado: TActionList;
    ac_Pesquisar: TAction;
    ac_Visualizar: TAction;
    ac_Incluir: TAction;
    ac_Fechar: TAction;
    dsPesqConsignado: TDataSource;
    sdsPesqConsignado: TSQLDataSet;
    dspPesqCosnignado: TDataSetProvider;
    cdsPesqConsignado: TClientDataSet;
    sdsPesqConsignadoVENDAID: TIntegerField;
    sdsPesqConsignadoCLIENTEID: TIntegerField;
    sdsPesqConsignadoUSUARIOID: TIntegerField;
    sdsPesqConsignadoDATA: TDateField;
    sdsPesqConsignadoCODTPMOVIMENTO: TIntegerField;
    sdsPesqConsignadoNOME: TStringField;
    sdsPesqConsignadoLOGIN: TStringField;
    sdsPesqConsignadoVALORTOTAL: TFloatField;
    sdsPesqConsignadoTOTALITENS: TFloatField;
    cdsPesqConsignadoVENDAID: TIntegerField;
    cdsPesqConsignadoCLIENTEID: TIntegerField;
    cdsPesqConsignadoUSUARIOID: TIntegerField;
    cdsPesqConsignadoDATA: TDateField;
    cdsPesqConsignadoCODTPMOVIMENTO: TIntegerField;
    cdsPesqConsignadoNOME: TStringField;
    cdsPesqConsignadoLOGIN: TStringField;
    cdsPesqConsignadoVALORTOTAL: TFloatField;
    cdsPesqConsignadoTOTALITENS: TFloatField;
    tbDevolucao: TTabSheet;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    GridDevolucao: TDBGrid;
    Label6: TLabel;
    edtClienteDev: TLabeledEdit;
    edtCodClienteDev: TLabeledEdit;
    GroupBox4: TGroupBox;
    GridPesqDevolucao: TDBGrid;
    Label8: TLabel;
    Label9: TLabel;
    DBText3: TDBText;
    DBText4: TDBText;
    cdsDevolucao: TClientDataSet ;
    StringField1: TStringField;
    IntegerField1: TIntegerField;
    StringField2: TStringField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    AggregateField1: TAggregateField;
    AggregateField2: TAggregateField;
    dsDevolucao: TDataSource;
    BitBtn5: TBitBtn;
    ac_Devolucao: TAction;
    lbConsignado: TLabel;
    lbNumConsignado: TLabel;
    GroupBox5: TGroupBox;
    cbxTpMovimento: TComboBox;
    Label7: TLabel;
    tbAcerto: TTabSheet;
    edtClienteAcerto: TLabeledEdit;
    edtCodClienteAcerto: TLabeledEdit;
    GridAcerto: TDBGrid;
    Panel3: TPanel;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    Label10: TLabel;
    Label11: TLabel;
    DBText5: TDBText;
    DBText6: TDBText;
    Label12: TLabel;
    dsAcerto: TDataSource;
    GroupBox6: TGroupBox;
    GridPesqAcerto: TDBGrid;
    Label13: TLabel;
    GroupBox2: TGroupBox;
    cbxPesquisa: TComboBox;
    edtPesquisa: TEdit;
    Label15: TLabel;
    cdsAcerto: TClientDataSet;
    StringField3: TStringField;
    IntegerField2: TIntegerField;
    StringField4: TStringField;
    FloatField4: TFloatField;
    FloatField5: TFloatField;
    FloatField6: TFloatField;
    AggregateField3: TAggregateField;
    AggregateField4: TAggregateField;
    ac_acerto: TAction;
    lbAcerto: TLabel;
    BitBtn8: TBitBtn;
    sdsPesqConsignadoCODORIGEM: TIntegerField;
    sdsPesqConsignadoSITUACAO: TStringField;
    cdsPesqConsignadoCODORIGEM: TIntegerField;
    cdsPesqConsignadoSITUACAO: TStringField;
    Label14: TLabel;
    BitBtn9: TBitBtn;
    ac_lancarCaixa: TAction;
    popupConsignado: TPopupMenu;
    EstornarCaixa1: TMenuItem;
    Cancelar1: TMenuItem;
    sdsPesqConsignadoCODACERTO: TIntegerField;
    cdsPesqConsignadoCODACERTO: TIntegerField;
    procedure cdsConsignadoCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure edtPessoaKeyPress(Sender: TObject; var Key: Char);
    procedure edtCodPessoaKeyPress(Sender: TObject; var Key: Char);
    procedure edtPesqProdutosKeyPress(Sender: TObject; var Key: Char);
    procedure edtPesqProdutosKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ac_FecharExecute(Sender: TObject);
    procedure ac_IncluirExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ac_VisualizarExecute(Sender: TObject);
    procedure ac_PesquisarExecute(Sender: TObject);
    procedure ac_DevolucaoExecute(Sender: TObject);
    procedure cdsDevolucaoCalcFields(DataSet: TDataSet);
    procedure GridPesqDevolucaoKeyPress(Sender: TObject; var Key: Char);
    procedure gridPesqProdutoKeyPress(Sender: TObject; var Key: Char);
    procedure ac_acertoExecute(Sender: TObject);
    procedure cdsAcertoCalcFields(DataSet: TDataSet);
    procedure GridPesqAcertoKeyPress(Sender: TObject; var Key: Char);
    procedure dbgConsignadoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ac_lancarCaixaExecute(Sender: TObject);
    procedure EstornarCaixa1Click(Sender: TObject);
    procedure Cancelar1Click(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaPessoa (ALabel:TLabeledEdit;AIndex: Integer);
    procedure BuscaProduto;
    procedure LimpaTela;
    procedure GravaConsignado;
    procedure GravaDevolucao;
    procedure AtualizaConsignado(AConsignado,ACodProduto,AQtde:string);
    procedure GravaAcerto;
    procedure LancaFinanceiro;
    procedure AtualizaSituacaoVenda(ACodVenda:Integer);
  public
    { Public declarations }
  end;

var
  FrmPesqConsignado: TFrmPesqConsignado;

implementation

{$R *.dfm}

uses uFrmPesqPessoas, uFrmPesqProduto, uDM, uFrmItem,
  uRotinas, uUsuario, uFrmPreFinanceiro, uFrmLancamentoCaixa;

procedure TFrmPesqConsignado.ac_acertoExecute(Sender: TObject);
var
 vendaid: string;
 QryPesquisa : TSQLQuery;
begin
 vendaid := cdsPesqConSignadoVENDAID.AsString;
 lbAcerto.Caption := vendaid;
 try
  QryPesquisa := TSQLQuery.Create(Self);
  QryPesquisa.SQLConnection := dm.DBAcesso;
  with QryPesquisa,SQL do
  begin
   Close;
   Clear;
   Add(' select vi.* ');
   Add(' from vw_vendas_pdv_itens vi  ');
   Add(' where vi.codtpmov = 2 ');
   Add(' and vi.vendaid = '+ vendaid);
   Open;
  end;
  edtCodClienteAcerto.Text := cdsPesqCoNSignado.FieldByName('CLIENTEID').AsString;
  edtClienteAcerto.Text := cdsPesqConsignado.FieldByName('NOME').AsString;
  edtCodClienteAcerto.ReadOnly := True;
  edtClienteAcerto.ReadOnly    := True;
  QryPesquisa.First;
  cdsConsignado.Open;
  while not QryPesquisa.Eof do
  begin
    cdsConsignado.Append;
    cdsConsignadoREFERENCIA.Value := QryPesquisa.FieldByName('REFERENCIA').Value;
    cdsConsignadoCODPRODUTO.Value := QryPesquisa.FieldByName('PRODUTOID').Value;
    cdsConsignadoDESCRICAO.Value  := QryPesquisa.FieldByName('DESCRICAO').Value;
    cdsConsignadoPRECO.Value      := QryPesquisa.FieldByName('PRECO').Value;
    cdsConsignadoVLTOTAL.Value    := QryPesquisa.FieldByName('VLTOTALPROD').Value;
    cdsConsignadoQTDE.Value       := QryPesquisa.FieldByName('QTDE').Value;
    cdsConsignado.Post;
    QryPesquisa.Next;
  end;
  PageControl1.ActivePage := tbAcerto;
  tbConsulta.TabVisible   := False;
  tbCadastro.TabVisible   := False;
  tbDevolucao.TabVisible  := False;
  tbAcerto.TabVisible     := True;
 finally
  FreeAndNil(QryPesquisa);
 end;
  cdsDevolucao.Open;

end;

procedure TFrmPesqConsignado.ac_DevolucaoExecute(Sender: TObject);
var
 vendaid: string;
 QryPesquisa : TSQLQuery;
begin
 vendaid := cdsPesqConsignadoVENDAID.AsString;
 lbNumConsignado.Caption := vendaid;
 try
  QryPesquisa := TSQLQuery.Create(Self);
  QryPesquisa.SQLConnection := dm.DBAcesso;
  with QryPesquisa,SQL do
  begin
   Close;
   Clear;
   Add(' select vi.* ');
   Add(' from vw_vendas_pdv_itens vi  ');
   Add(' where vi.codtpmov = 2 ');
   Add(' and vi.vendaid = '+ vendaid);
   Open;
  end;
  edtCodClienteDev.Text := cdsPesqConsignado.FieldByName('CLIENTEID').AsString;
  edtClienteDev.Text := cdsPesqConsignado.FieldByName('NOME').AsString;
  QryPesquisa.First;
  cdsConsignado.Open;
  while not QryPesquisa.Eof do
  begin
    cdsConsignado.Append;
    cdsConsignadoREFERENCIA.Value := QryPesquisa.FieldByName('REFERENCIA').Value;
    cdsConsignadoCODPRODUTO.Value := QryPesquisa.FieldByName('PRODUTOID').Value;
    cdsConsignadoDESCRICAO.Value  := QryPesquisa.FieldByName('DESCRICAO').Value;
    cdsConsignadoPRECO.Value      := QryPesquisa.FieldByName('PRECO').Value;
    cdsConsignadoVLTOTAL.Value    := QryPesquisa.FieldByName('VLTOTALPROD').Value;
    cdsConsignadoQTDE.Value       := QryPesquisa.FieldByName('QTDE').Value;
    cdsConsignado.Post;
    QryPesquisa.Next;
  end;
  PageControl1.ActivePage := tbDevolucao;
  tbConsulta.TabVisible   := False;
  tbCadastro.TabVisible   := False;
  tbDevolucao.TabVisible  := True;
  tbAcerto.TabVisible     := False;
 finally
  FreeAndNil(QryPesquisa);
 end;
  cdsDevolucao.Open;
end;

procedure TFrmPesqConsignado.ac_FecharExecute(Sender: TObject);
begin
 if PageControl1.ActivePage = tbConsulta then
 begin
  Close;
 end
 else
 begin
  LimpaTela;
  PageControl1.ActivePage := tbConsulta;
  tbConsulta.TabVisible   := True;
  tbCadastro.TabVisible   := False;
  tbDevolucao.TabVisible  := False;
  tbAcerto.TabVisible     := False;
 end;
end;

procedure TFrmPesqConsignado.ac_IncluirExecute(Sender: TObject);
begin
 if PageControl1.ActivePage = tbConsulta then
 begin
  PageControl1.ActivePage := tbCadastro;
  tbConsulta.TabVisible   := False;
  tbCadastro.TabVisible   := True;;
  tbDevolucao.TabVisible  := False;
  tbAcerto.TabVisible     := False;
 end
 else
 if PageControl1.ActivePage = tbCadastro then
   GravaConsignado
 else
 if PageControl1.ActivePage = tbDevolucao then
   GravaDevolucao
 else
   GravaAcerto;
end;

procedure TFrmPesqConsignado.ac_lancarCaixaExecute(Sender: TObject);
begin
 if cdsPesqConsignadoSITUACAO.Value <> 'ACE' then
 begin
   ShowMessage('N�o � possivel lan�ar caixa de um consignado que n�o esteja acertado');
   Exit;
 end;
 FrmLancamentoCaixa := TFrmLancamentoCaixa.Create(nil);
 FrmLancamentoCaixa.lbTotal.Caption := FormatFloat('##0.00',cdsPesqConsignadoVALORTOTAL.AsFloat);
 FrmLancamentoCaixa.IDCliente   := cdsPesqConsignadoCLIENTEID.Value;
 FrmLancamentoCaixa.NomeCliente := cdsPesqConsignadoNOME.AsString;
 FrmLancamentoCaixa.IDVenda     := cdsPesqConsignadoVENDAID.Value;
 if FrmLancamentoCaixa.ShowModal = mrOk then
 begin
   ShowMessage('Caixa Lan�ado com Sucesso!');
   FreeAndNil(FrmLancamentoCaixa);
   atualizaSituacaoVenda(cdsPesqConsignadoVENDAID.Value);
 end
 else
 begin
   FreeAndNil(FrmLancamentoCaixa);
 end;
 btnBuscar.Click;
end;

procedure TFrmPesqConsignado.ac_PesquisarExecute(Sender: TObject);
var
  S: TStringList;
begin
 try
  S := TStringList.Create;
  S.Add('  select v.vendaid,v.clienteid,v.usuarioid,v.data, v.codtpmovimento, v.codorigem, v.situacao,v.codacerto, ');
  S.Add('  c.nome, u.login, sum (i.preco * i.qtde) as valorTotal, sum(i.qtde) as TotalItens  ');
  S.Add('  from vendas v                                     ');
  S.Add('  join vendaitem i   on (v.vendaid = i.vendaid)     ');
  S.Add('  join produtos p    on (i.produtoid = p.produtoid) ');
  S.Add('  join clientes c    on (v.clienteid = c.clienteid) ');
  S.Add('  join usuarios u    on (v.usuarioid = u.usuarioid) ');
  if cbxTpMovimento.Text = 'CONSIGNADO' then
   S.Add(' where v.codtpmovimento = 2                         ')
  else if  cbxTpMovimento.Text = 'DEVOLUCAO' then
   S.Add(' where v.codtpmovimento = 3                         ')
  else if cbxTpMovimento.Text = 'ACERTO' then
   S.Add(' where v.codtpmovimento = 4                         ');

  S.Add('    and i.qtde > 0                                   ');

  if edtPesquisa.Text <> EmptyStr then
  begin
    if cbxPesquisa.ItemIndex = 0 then
      S.Add(' and c.nome like '+QuotedStr(edtPesquisa.Text+'%') )
    else
      S.Add(' and v.vendaid = '+ edtPesquisa.Text );
  end;

  S.Add('and v.data between '+ QuotedStr(FormatDateTime('DD.MM.YYYY',dtVendaI.Date))+ ' and ' + QuotedStr(FormatDateTime('DD.MM.YYYY',dtVendaF.Date))  );
  S.Add(' group by v.vendaid,v.clienteid,v.usuarioid,v.data, v.codtpmovimento, v.codorigem, v.situacao,v.codacerto,');
  S.Add('          c.clienteid, c.nome, u.usuarioid, u.login ');
  cdsPesqConsignado.Close;
  cdsPesqConsignado.CommandText := S.Text;
  cdsPesqConsignado.Open;
 finally
  FreeAndNil(S);
 end;
end;

procedure TFrmPesqConsignado.ac_VisualizarExecute(Sender: TObject);
var
 vendaid: string;
 QryPesquisa : TSQLQuery;
begin
 vendaid := cdsPesqConsignadoVENDAID.AsString;
 try
  QryPesquisa := TSQLQuery.Create(Self);
  QryPesquisa.SQLConnection := dm.DBAcesso;
  with QryPesquisa,SQL do
  begin
   Close;
   Clear;
   Add(' select vi.* ');
   Add(' from vw_vendas_pdv_itens vi  ');
   Add(' where vi.vendaid = '+ vendaid);
   Add(' and vi.qtde > 0 ');
   Open;
  end;
  edtCodPessoa.Text := cdsPesqConsignado.FieldByName('CLIENTEID').AsString;
  edtPessoa.Text := cdsPesqConsignado.FieldByName('NOME').AsString;
  QryPesquisa.First;
  cdsConsignado.Open;
  while not QryPesquisa.Eof do
  begin
    cdsConsignado.Append;
    cdsConsignadoREFERENCIA.Value := QryPesquisa.FieldByName('REFERENCIA').Value;
    cdsConsignadoCODPRODUTO.Value := QryPesquisa.FieldByName('PRODUTOID').Value;
    cdsConsignadoDESCRICAO.Value  := QryPesquisa.FieldByName('DESCRICAO').Value;
    cdsConsignadoPRECO.Value      := QryPesquisa.FieldByName('PRECO').Value;
    cdsConsignadoVLTOTAL.Value    := QryPesquisa.FieldByName('VLTOTALPROD').Value;
    cdsConsignadoQTDE.Value       := QryPesquisa.FieldByName('QTDE').Value;
    cdsConsignado.Post;
    QryPesquisa.Next;
  end;
  edtCodPessoa.ReadOnly := True;
  edtPessoa.ReadOnly    := True;
  GroupBox3.Visible := False;
  PageControl1.ActivePage := tbCadastro;
  tbConsulta.TabVisible  := False;
  tbCadastro.TabVisible  := True;
  tbDevolucao.TabVisible := False;
  tbAcerto.TabVisible    := False;
 finally
   FreeAndNil(QryPesquisa);
 end;

end;

procedure TFrmPesqConsignado.AtualizaConsignado(AConsignado, ACodProduto,
  AQtde: string);
begin
 with DM.QryTemp,SQL do
 begin
   Clear;
   Close;
   Add(' UPDATE VENDAITEM vi                ');
   Add(' SET vi.qtde  = '+AQTDE              );
   Add(' WHERE vi.vendaid = '+AConsignado    );
   Add(' AND  vi.produtoid = '+ ACodProduto  );
//   SaveToFile('C:\Andre\SQL.txt');
   ExecSQL(True);
 end;
end;

procedure TFrmPesqConsignado.AtualizaSituacaoVenda(ACodVenda: Integer);
begin
 with DM.QryTemp,SQL do
 begin
   Clear;
   Close;
   Add('update vendas v           ');
   Add('set v.situacao = ''LAN''  ');
   Add('where v.vendaid = ' + IntToStr(ACodVenda));
   ExecSQL(True);
 end;
end;

procedure TFrmPesqConsignado.BuscaPessoa(ALabel:TLabeledEdit;AIndex: Integer);
begin
//Passar o LabeledEdit e o ItemIdex (0- Codigo, 1-Nome)
 FrmPesqPessoa := TFrmPesqPessoa.Create(Self);
 FrmPesqPessoa.edtPesquisa.Text := TLabeledEdit(ALabel).Text;
 FrmPesqPessoa.cbxTpPesquisa.ItemIndex := AIndex;
 if FrmPesqPessoa.ShowModal = mrOk then
  begin
   edtCodPessoa.Text := DM.cdsPesquisaCliente.FieldByName('CLIENTEID').AsString;
   edtPessoa.Text := DM.cdsPesquisaCliente.FieldByName('NOME').AsString;
  end;
 FreeAndNil(FrmPesqPessoa);
end;

procedure TFrmPesqConsignado.BuscaProduto;
var
 S: TStringList;
begin
 try
  S:= TStringList.Create;
  dm.cdsConsultaProduto.Close;
  S.Add('SELECT p.produtoid,p.referencia, p.descricao, p.estoque, p.preco,');
  S.Add('       f.descricao, g.descricao');
  S.Add('from  produtos p, fabricante f, grupo g');
  S.Add('where p.codgrupo = g.cod');
  S.Add('  and p.codfabricante = f.cod');

  if cbxConsultaProduto.ItemIndex = 0 then
   S.Add('and p.descricao like '+QuotedStr(edtPesqProdutos.Text+'%'))
  else
   S.Add('and p.referencia like '+QuotedStr(edtPesqProdutos.Text));


  dm.cdsConsultaProduto.CommandText := S.Text;
  dm.cdsConsultaProduto.Open;
 finally
  FreeAndNil(S);
 end;

end;

procedure TFrmPesqConsignado.Cancelar1Click(Sender: TObject);
var
 Qry :TSQLQuery;
begin
 Qry := TSQLQuery.Create(nil);
 Qry.SQLConnection := DM.DBAcesso;

 if (cdsPesqConsignadoSITUACAO.Value <> 'ABE') then
 begin
   ShowMessage('N�o � possivel cancelar consignados que n�o est�o Aberto!');
   Exit;
 end;

 with DM.QryTemp,SQL do
 begin
  Clear;
  Close;
  Add(' update vendas v set v.situacao = ''CAN'' where v.vendaid = '+ IntToStr(cdsPesqConsignadoVENDAID.Value) );
  ExecSQL(True);
 end;
end;

procedure TFrmPesqConsignado.cdsAcertoCalcFields(DataSet: TDataSet);
begin
  cdsAcerto.FieldByName('VLTOTAL').Value := cdsAcerto.FieldByName('QTDE').Value * cdsAcerto.FieldByName('PRECO').Value
end;

procedure TFrmPesqConsignado.cdsConsignadoCalcFields(DataSet: TDataSet);
begin
 cdsConsignadoVLTOTAL.Value := cdsConsignadoPRECO.Value * cdsConsignadoQTDE.Value;
end;

procedure TFrmPesqConsignado.cdsDevolucaoCalcFields(DataSet: TDataSet);
begin
 cdsDevolucao.FieldByName('VLTOTAL').Value := cdsDevolucao.FieldByName('QTDE').Value * cdsDevolucao.FieldByName('PRECO').Value
end;

procedure TFrmPesqConsignado.dbgConsignadoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
 var
 Field : TField;
begin
 Field := Column.Field;
 if Field.FieldName = 'SITUACAO'  then
 begin
  if cdsPesqConsignadoSITUACAO.Value = 'LAN' then
  begin
    dbgConsignado.Canvas.Brush.Color := $db9e00;
    dbgConsignado.Canvas.Font.Color  := clWhite;
    dbgConsignado.Canvas.FillRect(Rect);
    dbgConsignado.DefaultDrawDataCell(Rect,dbgConsignado.Columns[DataCol].Field,State);
  end
  else if cdsPesqConsignadoSITUACAO.Value = 'ABE'  then
  begin
    dbgConsignado.Canvas.Brush.Color := $89da7f;
    dbgConsignado.Canvas.Font.Color  := clWhite;
    dbgConsignado.Canvas.FillRect(Rect);
    dbgConsignado.DefaultDrawDataCell(Rect,dbgConsignado.Columns[DataCol].Field,State);
  end
  else if cdsPesqConsignadoSITUACAO.Value = 'CAN'  then
  begin
    dbgConsignado.Canvas.Brush.Color := $4626ff;
    dbgConsignado.Canvas.Font.Color  := clWhite;
    dbgConsignado.Canvas.FillRect(Rect);
    dbgConsignado.DefaultDrawDataCell(Rect,dbgConsignado.Columns[DataCol].Field,State);
  end
  else if cdsPesqConsignadoSITUACAO.Value = 'ACE' then
  begin
    dbgConsignado.Canvas.Brush.Color := $65D3EE;
    dbgConsignado.Canvas.Font.Color  := clWhite;
    dbgConsignado.Canvas.FillRect(Rect);
    dbgConsignado.DefaultDrawDataCell(Rect,dbgConsignado.Columns[DataCol].Field,State);
  end;
 end;
end;

procedure TFrmPesqConsignado.GridPesqAcertoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
 begin
  if cdsConsignadoQTDE.Value > 0 then
  begin
   FrmItem := TFrmItem.Create(Self);
   FrmItem.edtQuantidade.Text := '1';
   FrmItem.edtPreco.Text := FormatFloat('##0.00',cdsConsignadoPRECO.Value);
   if FrmItem.ShowModal = mrOk then
   begin
    if StrToFloat(FrmItem.edtQuantidade.Text) > cdsConsignadoQTDE.Value then
    begin
      ShowMessage('Quantidade da devolu�ao n�o pode ser maior que quantidade total.');
      Exit;
    end;
    if cdsAcerto.Locate('REFERENCIA;PRECO',VarArrayOf([cdsConsignado.FieldByName('REFERENCIA').AsString,FrmItem.edtPreco.Text]),[]) then
    begin
     cdsAcerto.Edit;
     cdsConsignadoQTDE.Value := cdsConsignadoQTDE.Value - StrToFloat(FrmItem.edtQuantidade.Text);
     cdsAcerto.Post
    end
    else
    begin
     cdsAcerto.Append;
     cdsConsignado.Edit;
     cdsAcerto.FieldByName('REFERENCIA').Value := cdsConsignadoREFERENCIA.Value;
     cdsAcerto.FieldByName('CODPRODUTO').Value := cdsConsignadoCODPRODUTO.Value;
     cdsAcerto.FieldByName('PRECO').Value      := StrToFloat(FrmItem.edtPreco.Text);
     cdsAcerto.FieldByName('QTDE').Value       := StrToFloat(FrmItem.edtQuantidade.Text);
     cdsAcerto.FieldByName('VLTOTAL').Value    := cdsConsignadoVLTOTAL.Value;
     cdsAcerto.Post;
     cdsConsignadoQTDE.Value := cdsConsignadoQTDE.Value - StrToFloat( FrmItem.edtQuantidade.Text);
     cdsConsignado.Post;
    end;
   end;
   FreeAndNil(FrmItem);
  end
  else
  begin
    ShowMessage('N�o h� mais itens para essa referencia');
    Exit;
  end;
 end;
end;

procedure TFrmPesqConsignado.GridPesqDevolucaoKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = #13 then
 begin
  if cdsConsignadoQTDE.Value > 0 then
  begin
   FrmItem := TFrmItem.Create(Self);
   FrmItem.edtQuantidade.Text := '1';
   FrmItem.edtPreco.Text := FormatFloat('##0.00',cdsConsignadoPRECO.Value);
   if FrmItem.ShowModal = mrOk then
   begin
    if StrToFloat(FrmItem.edtQuantidade.Text) > cdsConsignadoQTDE.Value then
    begin
      ShowMessage('Quantidade da devolu�ao n�o pode ser maior que quantidade total.');
      Exit;
    end;
    if cdsDevolucao.Locate('REFERENCIA;PRECO',VarArrayOf([cdsConsignado.FieldByName('REFERENCIA').AsString,FrmItem.edtPreco.Text]),[]) then
    begin
     cdsDevolucao.Edit;
     cdsConsignadoQTDE.Value := cdsConsignadoQTDE.Value - StrToFloat(FrmItem.edtQuantidade.Text);
     cdsDevolucao.Post
    end
    else
    begin
     cdsDevolucao.Append;
     cdsConsignado.Edit;
     cdsDevolucao.FieldByName('REFERENCIA').Value := cdsConsignadoREFERENCIA.Value;
     cdsDevolucao.FieldByName('CODPRODUTO').Value := cdsConsignadoCODPRODUTO.Value;
     cdsDevolucao.FieldByName('PRECO').Value      := cdsConsignadoPRECO.Value;
     cdsDevolucao.FieldByName('QTDE').Value       := StrToInt( FrmItem.edtQuantidade.Text);
     cdsDevolucao.FieldByName('VLTOTAL').Value    := cdsConsignadoVLTOTAL.Value;
     cdsDevolucao.Post;
     cdsConsignadoQTDE.Value := cdsConsignadoQTDE.Value - StrToFloat( FrmItem.edtQuantidade.Text);
     cdsConsignado.Post;
    end;
   end;
   FreeAndNil(FrmItem);
  end
  else
  begin
    ShowMessage('N�o h� mais itens para essa referencia');
    Exit;
  end;
 end;

end;


procedure TFrmPesqConsignado.gridPesqProdutoKeyPress(Sender: TObject;
  var Key: Char);
begin
  if Key = #13 then
  begin
   FrmItem := TFrmItem.Create(Self);
   FrmItem.edtQuantidade.Text := '1';
   FrmItem.edtPreco.Text := FormatFloat('##0.00',DM.cdsConsultaProdutoPRECO.Value);
   if FrmItem.ShowModal = mrOk then
   begin
    if cdsConsignado.Locate('REFERENCIA;PRECO',VarArrayOf([dm.cdsConsultaProduto.FieldByName('REFERENCIA').AsString,FrmItem.edtPreco.Text]),[]) then
    begin
      cdsConsignado.Edit;
      cdsConsignadoQTDE.Value := cdsConsignadoQTDE.Value + StrToFloat(FrmItem.edtQuantidade.Text);
      cdsConsignado.Post
    end
    else
    begin
     cdsConsignado.Append;
     cdsConsignadoCODPRODUTO.Value := DM.cdsConsultaProduto.FieldByName('PRODUTOID').Value;
     cdsConsignadoREFERENCIA.AsString := dm.cdsConsultaProduto.FieldByName('REFERENCIA').AsString;
     cdsConsignadoDESCRICAO.AsString := dm.cdsConsultaProduto.FieldByName('DESCRICAO').AsString;
     cdsConsignadoPRECO.AsString := FrmItem.edtPreco.Text;
     cdsConsignadoQTDE.AsFloat := StrToFloat(FrmItem.edtQuantidade.Text);
     cdsConsignado.Post;
    end;
   end;
   edtPesqProdutos.SelectAll;
   edtPesqProdutos.SetFocus;
  end;
end;

procedure TFrmPesqConsignado.edtCodPessoaKeyPress(Sender: TObject;
  var Key: Char);
begin
 if Key = #13 then
  BuscaPessoa(edtCodPessoa,0);
end;

procedure TFrmPesqConsignado.edtPesqProdutosKeyPress(Sender: TObject;
  var Key: Char);
begin
 if Key = #13 then
  gridPesqProduto.SetFocus;
end;

procedure TFrmPesqConsignado.edtPesqProdutosKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   BuscaProduto;
end;

procedure TFrmPesqConsignado.edtPessoaKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = #13 then
 begin
   BuscaPessoa(edtPessoa,1);
 end;
end;

procedure TFrmPesqConsignado.EstornarCaixa1Click(Sender: TObject);
begin
 if cdsPesqConsignadoSITUACAO.Value <> 'LAN' then
 begin
   ShowMessage('N�o � possivel estornar consignado que n�o esteja lan�ado no caixa');
   Exit;
 end;
 try
  with DM.QryTemp,SQL do
  begin
    //Estorna o lan�amento
    Clear;
    Close;
    Add(' update fin_acerto a set a.status = ''EST'' where a.cod = '+ IntToStr(cdsPesqConsignadoCODACERTO.Value) );
    ExecSQL(True);

    //Volta a venda para Acertado
    Clear;
    Close;
    Add(' update vendas v set v.situacao = ''ACE'' where v.vendaid = '+ IntToStr(cdsPesqConsignadoVENDAID.Value) );
    ExecSQL(True);
  end;
  ShowMessage('Estorno realizado com sucesso!');
 except
  ShowMessage('Falha ao estornar caixa!');
 end;

end;

procedure TFrmPesqConsignado.FormCreate(Sender: TObject);
begin
 cdsConsignado.CreateDataSet;
 cdsDevolucao.CreateDataSet;
 cdsAcerto.CreateDataSet;
 PageControl1.ActivePage := tbConsulta;
 tbConsulta.TabVisible   := True;
 tbCadastro.TabVisible   := False;
 tbDevolucao.TabVisible  := False;
 tbAcerto.TabVisible     := False;
end;

procedure TFrmPesqConsignado.FormShow(Sender: TObject);
begin
 PageControl1.ActivePage := tbConsulta;
 dtVendaI.Date := Now - 30;
 dtVendaF.Date := Now;
end;

procedure TFrmPesqConsignado.GravaAcerto;
var
 CodVenda:Integer;
begin
 if (edtCodClienteAcerto.Text = EmptyStr) or ( edtClienteAcerto.Text = EmptyStr) then
 begin
   ShowMessage('Voc� deve incluir uma pessoa!');
   edtPessoa.SetFocus;
   Exit;
 end;
 if cdsConsignado.IsEmpty then
 begin
   ShowMessage('Voc� deve incluir pelo menos 1 produto.');
   Exit;
 end;
 try
  with DM do
  begin
   cdsVendas.Open;
   cdsVendas.Append;
   CodVenda := cdsVendasVENDAID.Value;
   cdsVendasCLIENTEID.Value       := StrToInt(edtCodClienteAcerto.Text);
   cdsVendasUSUARIOID.Value       := TUsuario.GetInstance.ID;
   cdsVendasDATA.Value            := Now;
   cdsVendasCODTPMOVIMENTO.Value  := 4;
   cdsVendasSITUACAO.Value        := 'ACE';
   cdsVendasCODORIGEM.Value       := StrToInt(lbAcerto.Caption);
   cdsAcerto.First;
   while not cdsAcerto.Eof do
   begin
     cdsVendaItens.Append;
     cdsVendaItens.FieldByName('PRODUTOID').Value  := cdsAcerto.FieldByName('CODPRODUTO').Value;
     cdsVendaItens.FieldByName('QTDE').Value       := cdsAcerto.FieldByName('QTDE').Value;
     cdsVendaItens.FieldByName('PRECO').Value      := cdsAcerto.FieldByName('PRECO').Value;
     cdsVendaItens.Post;
     cdsAcerto.Next;
   end;
   cdsVendas.ApplyUpdates(0);
   GravarLog(cdsVendasVENDAID.value,'ACERTO CONSG N:'+lbAcerto.Caption+' Cli:'+edtCodClienteAcerto.Text);
   cdsVendas.Close;

  end;
  cdsAcerto.First;
  while not cdsAcerto.Eof do
  begin
   AtualizaConsignado(lbAcerto.Caption,cdsConsignadoCODPRODUTO.AsString,cdsConsignadoQTDE.AsString);
   cdsAcerto.Next;
  end;

  if MessageDlg('Deseja lan�ar no caixa?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
  begin
    Application.CreateForm(TFrmLancamentoCaixa,FrmLancamentoCaixa);
    FrmLancamentoCaixa.lbTotal.Caption := cdsAcerto.FieldByName('SUBTOTAL').AsString;
    FrmLancamentoCaixa.IDCliente       := StrToInt(edtCodClienteAcerto.Text);
    FrmLancamentoCaixa.NomeCliente     := edtClienteAcerto.Text;
    FrmLancamentoCaixa.IDVenda         := CodVenda;
    if FrmLancamentoCaixa.ShowModal = mrOk then
    begin
      ShowMessage('Caixa Lan�ado com Sucesso!');
      FreeAndNil(FrmLancamentoCaixa);
      AtualizaSituacaoVenda(CodVenda);
    end
    else
    begin
      FreeAndNil(FrmLancamentoCaixa);
    end;
  end;

  GravarLog(CodVenda,'Acerto Consig');
  LimpaTela;
 except
  DM.cdsVendas.CancelUpdates;
 end;
end;

procedure TFrmPesqConsignado.GravaConsignado;
begin
  if (edtCodPessoa.Text = EmptyStr) or ( edtPessoa.Text = EmptyStr) then
  begin
    ShowMessage('Voc� deve incluir uma pessoa!');
    edtPessoa.SetFocus;
    Exit;
  end;
  if cdsConsignado.IsEmpty then
  begin
    ShowMessage('Voc� deve incluir pelo menos 1 produto.');
    Exit;
  end;
 try
  with DM do
  begin
   cdsVendas.Open;
   cdsVendas.Append;
   cdsVendasCLIENTEID.Value       := StrToInt(edtCodPessoa.Text);
   cdsVendasUSUARIOID.Value       := TUsuario.GetInstance.ID;
   cdsVendasDATA.Value            := Now;
   cdsVendasSITUACAO.Value        := 'ABE';
   cdsVendasCODTPMOVIMENTO.Value  := 2;
   cdsConsignado.First;
   while not cdsConsignado.Eof do
   begin
     cdsVendaItens.Append;
     cdsVendaItens.FieldByName('PRODUTOID').Value  := cdsConsignadoCODPRODUTO.Value;
     cdsVendaItens.FieldByName('QTDE').Value       := cdsConsignadoQTDE.Value;
     cdsVendaItens.FieldByName('PRECO').Value      := cdsConsignadoPRECO.Value;
     cdsVendaItens.Post;
     cdsConsignado.Next;
   end;
   cdsVendas.ApplyUpdates(0);
   GravarLog(cdsVendasVENDAID.Value,'CONSIGNADO CLI:'+edtCodPessoa.Text);
   cdsVendas.Close;
  end;
  LimpaTela;
 except
  DM.cdsVendas.CancelUpdates;
 end;
end;

procedure TFrmPesqConsignado.GravaDevolucao;
begin
 try
  with DM do
  begin
   cdsVendas.Open;
   cdsVendas.Append;
   cdsVendasCLIENTEID.Value       := StrToInt(edtCodClienteDev.Text);
   cdsVendasUSUARIOID.Value       := TUsuario.GetInstance.ID;
   cdsVendasDATA.Value            := Now;
   cdsVendasCODTPMOVIMENTO.Value  := 3;
   cdsVendasSITUACAO.Value        := 'DEV';
   cdsVendasCODORIGEM.Value       := StrToInt(lbNumConsignado.Caption);
   cdsDevolucao.First;
   while not cdsDevolucao.Eof do
   begin
    cdsVendaItens.Append;
    cdsVendaItens.FieldByName('PRODUTOID').Value  := cdsDevolucao.FieldByName('CODPRODUTO').Value;
    cdsVendaItens.FieldByName('QTDE').Value       := cdsDevolucao.FieldByName('QTDE').Value;
    cdsVendaItens.FieldByName('PRECO').Value      := cdsDevolucao.FieldByName('PRECO').Value;
    cdsVendaItens.Post;
    cdsDevolucao.Next;
   end;
   cdsVendas.ApplyUpdates(0);
  end;
  cdsConsignado.First;
  while not cdsConsignado.Eof do
  begin
   AtualizaConsignado(lbNumConsignado.Caption,cdsConsignadoCODPRODUTO.AsString,cdsConsignadoQTDE.AsString);
   cdsConsignado.Next;
  end;

  GravarLog(StrToInt(lbNumConsignado.Caption),'DEVOLUCAO CONSG N:'+lbAcerto.Caption+' Cli:'+edtCodClienteAcerto.Text);
 except
  DM.cdsVendas.CancelUpdates;
 end;
 LimpaTela;

end;

procedure TFrmPesqConsignado.LancaFinanceiro;
begin
  FrmPreFinanceiro := TFrmPreFinanceiro.Create(nil);
  with FrmPreFinanceiro do
  begin
   edtCodPessoa.Text     := edtCodClienteAcerto.Text;
   edtPessoa.Text        := edtClienteAcerto.Text;
   edtCodPessoa.ReadOnly := True;
   edtPessoa.ReadOnly    := True;
   edtDocumento.Text     := 'ACERTO: '+ lbAcerto.Caption;
   setCodMov(StrToInt(lbAcerto.Caption));
   edtValor.Text         := FloatToStr(cdsAcerto.FieldByName('SUBTOTAL').Value);
   dtVencimento.Date     := Now + 30;
   ShowModal;
  end;
  FreeAndNil(FrmPreFinanceiro);
end;

procedure TFrmPesqConsignado.LimpaTela;
begin
 edtCodPessoa.Clear;
 edtPessoa.Clear;
 edtPesqProdutos.Clear;
 PageControl1.ActivePage := tbConsulta;
 edtCodClienteDev.Clear;
 edtClienteDev.Clear;
 if DM.cdsConsultaProduto.Active then
   DM.cdsConsultaProduto.Close;
 if cdsConsignado.Active then
  cdsConsignado.EmptyDataSet;
 if cdsDevolucao.Active then
  cdsDevolucao.EmptyDataSet;
 if cdsAcerto.Active then
  cdsAcerto.EmptyDataSet;
 GroupBox3.Visible := True;
end;

end.
