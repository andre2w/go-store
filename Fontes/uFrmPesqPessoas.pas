unit uFrmPesqPessoas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.StdCtrls, Vcl.Buttons;

type
  TFrmPesqPessoa = class(TForm)
    GroupBox1: TGroupBox;
    Label6: TLabel;
    btnPesquisar: TBitBtn;
    edtPesquisa: TLabeledEdit;
    cbxTpPesquisa: TComboBox;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    btnConfirmar: TBitBtn;
    btnCancelar: TBitBtn;
    dsPesqPessoa: TDataSource;
    procedure btnPesquisarClick(Sender: TObject);
    procedure edtPesquisaKeyPress(Sender: TObject; var Key: Char);
    procedure btnConfirmarClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    procedure BuscarPessoa;
  public
    { Public declarations }
  end;

var
  FrmPesqPessoa: TFrmPesqPessoa;

implementation

{$R *.dfm}

uses uDM;

procedure TFrmPesqPessoa.btnCancelarClick(Sender: TObject);
begin
 ModalResult := mrCancel;
 Close;
end;

procedure TFrmPesqPessoa.btnConfirmarClick(Sender: TObject);
begin
 ModalResult := mrOk;
end;

procedure TFrmPesqPessoa.btnPesquisarClick(Sender: TObject);
begin
 BuscarPessoa;
end;

procedure TFrmPesqPessoa.BuscarPessoa;
var
 S: TStringList;
begin
 S := TStringList.Create;
 S.Add(' Select c.clienteid, c.nome, c.endereco, c.bairro, c.telefone, c.cidade ');
 S.Add(' from clientes c');

 if cbxTpPesquisa.Text = 'CODIGO' then
  S.Add(' where c.clienteid = ' + edtPesquisa.Text )
 ELSE
 if cbxTpPesquisa.Text = 'NOME' then
  S.Add(' where c.nome like '+ QuotedStr(edtPesquisa.Text+'%') )
 else
  S.Add(' where c.cidade like ' + QuotedStr(edtPesquisa.Text+'%') );

 dm.cdsPesquisaCliente.Close;
 dm.cdsPesquisaCliente.CommandText := S.Text;
 dm.cdsPesquisaCliente.Open;
 DBGrid1.SetFocus;
end;

procedure TFrmPesqPessoa.DBGrid1DblClick(Sender: TObject);
begin
 ModalResult := mrOk;
end;

procedure TFrmPesqPessoa.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
 if Key = #13 then
  ModalResult := mrOk;
end;

procedure TFrmPesqPessoa.edtPesquisaKeyPress(Sender: TObject; var Key: Char);
begin
 if key = #13 then
  BuscarPessoa;
end;

procedure TFrmPesqPessoa.FormShow(Sender: TObject);
begin
 btnPesquisar.Click;
end;

end.
