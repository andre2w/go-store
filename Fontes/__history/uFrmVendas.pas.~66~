unit uFrmVendas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Data.DB, Datasnap.DBClient, Vcl.DBCtrls,
  Vcl.ActnList, Vcl.Mask, System.Actions, Edit2w, Data.SqlExpr;

type
  TProduto = record
    PRODUTOID  : INTEGER;
    REFERENCIA : string;
    DESCRICAO  : string;
    QTDE       : Double;
    PRECO      : Double;
  end;
  TFrmVendas = class(TForm)
    dsConsultaProduto: TDataSource;
    cdsItems: TClientDataSet;
    dsItems: TDataSource;
    cdsItemsCODIGO: TIntegerField;
    cdsItemsDESCRICAO: TStringField;
    cdsItemsQTDE: TFloatField;
    cdsItemsPRECO: TFloatField;
    cdsItemsValorTotal: TFloatField;
    cdsItemsREFERENCIA: TStringField;
    ActionListPDV: TActionList;
    PDV_Buscar: TAction;
    PDV_Fechar: TAction;
    cdsItemsTotalItens: TAggregateField;
    cdsItemsSubTotal: TAggregateField;
    PDV_Finalizar: TAction;
    PDV_Limpar: TAction;
    GroupBox1: TGroupBox;
    cbTpPesquisa: TComboBox;
    edQtde: TEdit;
    edPreco: TEdit;
    Label8: TLabel;
    Label9: TLabel;
    edProduto: TEdit;
    GroupBox2: TGroupBox;
    edCliente: TEdit2w;
    Label4: TLabel;
    txtSubTotal: TDBText;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    edPercDesconto: TEdit;
    edDesconto: TEdit;
    lbSubTotal: TLabel;
    btnFechar: TBitBtn;
    btnFinalizar: TBitBtn;
    btnLimpar: TBitBtn;
    Label7: TLabel;
    GroupBox3: TGroupBox;
    DBGrid2: TDBGrid;
    Label1: TLabel;
    txtTotalItens: TDBText;
    cbLancarCaixa: TCheckBox;
    procedure BtnBuscarClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cdsItemsCalcFields(DataSet: TDataSet);
    procedure PDV_FecharExecute(Sender: TObject);
    procedure PDV_FinalizarExecute(Sender: TObject);
    procedure PDV_LimparExecute(Sender: TObject);
    procedure edPrecoKeyPress(Sender: TObject; var Key: Char);
    procedure edPercDescontoKeyPress(Sender: TObject; var Key: Char);
    procedure edDescontoKeyPress(Sender: TObject; var Key: Char);
    procedure edProdutoKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edClienteAfterSearch(Sender: TObject);
    procedure edClienteDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure FormataMoeda(Componente: TObject; var Key: Char);
    procedure IncluirProduto(AProduto:TProduto);
    procedure LimparProduto;
    procedure CalculaValorTotal;
    function  CalculaDesconto:Double;
    procedure CalculaDescontoReais;
    procedure atualizaSituacaoVenda(ACodVenda: Integer);
  protected
    procedure ConsultaProduto;
  public
    { Public declarations }
  end;

var
  FrmVendas: TFrmVendas;
  ValorTotalVenda: Extended;
  Produto : TProduto;
implementation

{$R *.dfm}

uses uDM, uFrmItem, uFrmFinalizaVenda, uFrmPesqProduto, uFrmLancamentoCaixa,
  uRotinas, uUsuario;

{ TFrmVendas }

procedure TFrmVendas.atualizaSituacaoVenda(ACodVenda: Integer);
begin
 with DM.QryTemp,SQL do
 begin
   Clear;
   Close;
   Add('update vendas v           ');
   Add('set v.situacao = ''LAN''  ');
   Add('where v.vendaid = ' + IntToStr(ACodVenda));
   ExecSQL(True);
 end;
end;

procedure TFrmVendas.BtnBuscarClick(Sender: TObject);
begin
 ConsultaProduto;
end;

procedure TFrmVendas.btnFecharClick(Sender: TObject);
begin
 Close;
end;

function TFrmVendas.CalculaDesconto: Double;
var
 vlDesconto, percDesconto : Double;
begin
 percDesconto := StrToFloat(edPercDesconto.Text);
 vlDesconto := ( cdsItemsSubTotal.Value * percDesconto) / 100 ;
 Result := vlDesconto;
end;

procedure TFrmVendas.CalculaDescontoReais;
var
  valorDescontoPerc,valorDescontoReais:Double;
begin
  valorDescontoReais := StrToFloat(edDesconto.Text);
  valorDescontoPerc := ( valorDescontoReais / cdsItemsSubTotal.Value ) * 100;
  edPercDesconto.Text := FormatFloat('##0.000',valorDescontoPerc);
  CalculaValorTotal;
end;

procedure TFrmVendas.CalculaValorTotal;
var
 vltotal,desconto : double;
begin
 desconto := CalculaDesconto;
 vltotal := cdsItemsSubTotal.Value - desconto;
 edDesconto.Text := FormatFloat('##0.00',desconto);
 lbSubTotal.Caption := 'R$'+ FormatFloat('###,##0.00',vltotal);
end;

procedure TFrmVendas.cdsItemsCalcFields(DataSet: TDataSet);
begin
  cdsItemsValorTotal.Value := cdsItemsQTDE.Value * cdsItemsPRECO.Value;
end;

procedure TFrmVendas.ConsultaProduto;
begin
 FrmPesqProduto := TFrmPesqProduto.Create(Self);
 FrmPesqProduto.edtPesquisaProduto.Text := edProduto.Text;
 FrmPesqProduto.cbxTpPesquisa.Text      := cbTpPesquisa.Text;
 FrmPesqProduto.PesquisaProduto;
 if DM.cdsConsultaProduto.RecordCount = 1 then
 begin
  Produto.PRODUTOID  := dm.cdsConsultaProdutoPRODUTOID.Value;
  Produto.REFERENCIA := dm.cdsConsultaProdutoREFERENCIA.Value;
  Produto.DESCRICAO  := dm.cdsConsultaProdutoDESCRICAO.Value;
  Produto.QTDE       := 1;
  Produto.PRECO      := DM.cdsConsultaProdutoPRECO.Value;
 end
 else
 if FrmPesqProduto.ShowModal = mrOk then
 begin
  Produto.PRODUTOID  := dm.cdsConsultaProdutoPRODUTOID.Value;
  Produto.REFERENCIA := dm.cdsConsultaProdutoREFERENCIA.Value;
  Produto.DESCRICAO  := dm.cdsConsultaProdutoDESCRICAO.Value;
  Produto.QTDE       := 1;
  Produto.PRECO      := DM.cdsConsultaProdutoPRECO.Value;
 end;
 edQtde.Text := '1';
 edPreco.Text := FormatFloat('###,##0.00',Produto.PRECO);
 edProduto.Text := Produto.DESCRICAO;
 edQtde.SetFocus;

end;

procedure TFrmVendas.edClienteAfterSearch(Sender: TObject);
begin
 if edCliente.Text = 'NAO ENCONTRADO...' then
 begin
   edCliente.SetFocus;
   edCliente.SelectAll;
 end
 else
  edCliente.ReadOnly := True;
end;

procedure TFrmVendas.edClienteDblClick(Sender: TObject);
begin
 if edCliente.ReadOnly = True then
 begin
   edCliente.ReadOnly := False;
   edCliente.SelectAll;
   edCliente.SetFocus;
 end;
end;

procedure TFrmVendas.edDescontoKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
    CalculaDescontoReais
  else
    FormataMoeda(Sender,Key);
end;

procedure TFrmVendas.edPercDescontoKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = #13 then
  CalculaValorTotal
 else
  FormataMoeda(Sender,Key);
end;

procedure TFrmVendas.edPrecoKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = #13 then
 begin
  Produto.QTDE  := StrToFloat(edQtde.Text);
  Produto.PRECO := StrToFloat(edPreco.Text);
  IncluirProduto(Produto);
  LimparProduto;
  CalculaValorTotal;
  edProduto.Clear;
  edQtde.Clear;
  edPreco.Clear;
  edProduto.SetFocus;
 end
 else
  FormataMoeda(Sender,Key);
end;

procedure TFrmVendas.edProdutoKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = #13 then
  ConsultaProduto;
end;

procedure TFrmVendas.FormataMoeda(Componente: TObject; var Key: Char);
var
   str_valor  : String;
   dbl_valor  : double;
begin
    { verificando se estamos recebendo o TEdit realmente }
   IF (Componente is TLabeledEdit) or
      (Componente is TEdit) THEN
   BEGIN
      { se tecla pressionada � um numero, backspace ou del deixa passar }
      IF ( Key in ['0'..'9', #8, #9] ) THEN
      BEGIN
         { guarda valor do TEdit com que vamos trabalhar }
         str_valor := TEdit( Componente ).Text ;
         { verificando se nao esta vazio }
         IF str_valor = EmptyStr THEN str_valor := '0,00' ;
         { se valor numerico ja insere na string temporaria }
         IF Key in ['0'..'9'] THEN str_valor := Concat( str_valor, Key ) ;
         { retira pontos e virgulas se tiver! }
         str_valor := Trim( StringReplace( str_valor, '.', '', [rfReplaceAll, rfIgnoreCase] ) ) ;
         str_valor := Trim( StringReplace( str_valor, ',', '', [rfReplaceAll, rfIgnoreCase] ) ) ;
         {inserindo 2 casas decimais}
         dbl_valor := StrToFloat( str_valor ) ;
         dbl_valor := ( dbl_valor / 100 ) ;

         {reseta posicao do tedit}
         TEdit( Componente ).SelStart := Length( TEdit( Componente ).Text );
         {retornando valor tratado ao TEdit}
         TEdit( Componente ).Text := FormatFloat( '#####0.00', dbl_valor ) ;
      END;
      {se nao e' key relevante entao reseta}
      IF NOT( Key in [#8, #9] ) THEN key := #0;
   END;

end;

procedure TFrmVendas.FormCreate(Sender: TObject);
begin
 cdsItems.CreateDataSet;
 UsuID := TUsuario.GetInstance.ID;
end;


procedure TFrmVendas.FormKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = #13 then
  Perform(WM_NEXTDLGCTL, 0, 0);
 if Key = #27 then
  PDV_Fechar.Execute;
end;

procedure TFrmVendas.FormShow(Sender: TObject);
begin
 edCliente.SetFocus;
end;

procedure TFrmVendas.IncluirProduto(AProduto: TProduto);
begin
  if cdsItems.Locate('CODIGO;PRECO',VarArrayOf([AProduto.PRODUTOID,AProduto.PRECO]),[]) then
  begin
   cdsItems.Edit;
   cdsItemsQTDE.AsFloat := cdsItemsQTDE.Value + AProduto.QTDE;
   cdsItems.Post
  end
  else
  begin
   cdsItems.Append;
   cdsItemsCODIGO.Value := AProduto.PRODUTOID;
   cdsItemsREFERENCIA.Value := AProduto.REFERENCIA;
   cdsItemsDESCRICAO.Value := AProduto.DESCRICAO;
   cdsItemsPRECO.Value := AProduto.PRECO;
   cdsItemsQTDE.Value := AProduto.QTDE;
   cdsItems.Post;
  end;
end;

procedure TFrmVendas.LimparProduto;
begin
 Produto.PRODUTOID  := 0;
 Produto.REFERENCIA := '';
 Produto.DESCRICAO  := '';
 Produto.QTDE       := 0;
 Produto.PRECO      := 0;
end;

procedure TFrmVendas.PDV_FecharExecute(Sender: TObject);
begin
 Dm.cdsConsultaProduto.Close;
 Close;
end;

procedure TFrmVendas.PDV_FinalizarExecute(Sender: TObject);
begin
 if (edCliente.Text = EmptyStr) or (edCliente.Result = EmptyStr)  then
 begin
  ShowMessage('Voc� precisa selecionar um Cliente');
  Exit;
 end;

 if cdsItems.IsEmpty then
 begin
   ShowMessage('N�o h� itens lan�ados para esta venda!');
   Exit;
 end;

 with DM do
 begin
  if not cdsVendas.Active then
    cdsVendas.Open;

  cdsVendas.Append;
  cdsVendasCLIENTEID.Value      := StrToInt(edCliente.Result);
  cdsVendasUSUARIOID.Value      := UsuID;
  cdsVendasDATA.Value           := Date;
  cdsVendasCODTPMOVIMENTO.Value := 1;
  cdsVendasPERCDESCONTO.Value   := StrToFloat(edPercDesconto.Text);
  cdsVendasSITUACAO.Value       := 'ABE';

  cdsItems.First;
  while not cdsItems.Eof do
  begin
   cdsVendaItens.Append;
   cdsVendaItens.FieldByName('VENDAID').Value    := cdsVendasVENDAID.Value;
   cdsVendaItens.FieldByName('PRODUTOID').Value  := cdsItemsCODIGO.Value;
   cdsVendaItens.FieldByName('QTDE').Value       := cdsItemsQTDE.Value;
   cdsVendaItens.FieldByName('PRECO').Value      := cdsItemsPRECO.Value;
   cdsVendaItens.Post;
   cdsItems.Next;
  end;
  cdsVendas.ApplyUpdates(0);

  if cbLancarCaixa.Checked then
  begin
   FrmLancamentoCaixa := TFrmLancamentoCaixa.Create(nil);
   FrmLancamentoCaixa.lbTotal.Caption := FormatFloat('##0.00',cdsItemsSubTotal.Value); // POG PARA FORMATAR O VALOR CORRETAMENTE
   FrmLancamentoCaixa.IDCliente   := StrToInt(edCliente.Result);
   FrmLancamentoCaixa.IDVenda     := cdsVendasVENDAID.Value;
   if FrmLancamentoCaixa.ShowModal = mrOk then
   begin
     ShowMessage('Caixa Lan�ado com Sucesso!');
     FreeAndNil(FrmLancamentoCaixa);
     atualizaSituacaoVenda(cdsVendasVENDAID.Value);
   end
   else
   begin
     FreeAndNil(FrmLancamentoCaixa);
   end;
  end;
  GravarLog(cdsVendasVENDAID.Value,'PDV');
  cdsVendas.Close;
  FrmVendas.cdsItems.EmptyDataSet;
  Close;
  end;

end;

procedure TFrmVendas.PDV_LimparExecute(Sender: TObject);
begin
 if MessageDlg('Deseja Limpar os Itens?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
 begin
   cdsItems.EmptyDataSet;
   edDesconto.Text := '0,00';
   edPercDesconto.Text := '0,00';
   CalculaValorTotal;
 end;
end;

end.
