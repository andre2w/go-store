unit uFrmLancamentoCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons;

type
  TFrmLancamentoCaixa = class(TForm)
    Label1: TLabel;
    edDinheiro: TEdit;
    edCheque: TEdit;
    lbCheque: TLabel;
    edCredito: TEdit;
    Label3: TLabel;
    edDebito: TEdit;
    lbDebito: TLabel;
    btnConfirmar: TBitBtn;
    btnCancelar: TBitBtn;
    cbxCC: TComboBox;
    lbTotal: TLabel;
    Label2: TLabel;
    lbRecebido: TLabel;
    Label5: TLabel;
    procedure edDinheiroKeyPress(Sender: TObject; var Key: Char);
    procedure edChequeKeyPress(Sender: TObject; var Key: Char);
    procedure edCreditoKeyPress(Sender: TObject; var Key: Char);
    procedure edDebitoKeyPress(Sender: TObject; var Key: Char);
    procedure btnConfirmarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure formataMoeda(Componente: TObject; Var key: Char);
    procedure getCC;
    procedure calculaValorRecebido;
  public
    { Public declarations }
  end;

var
  FrmLancamentoCaixa: TFrmLancamentoCaixa;
  vlTotal : Double;
implementation

{$R *.dfm}

uses uDM, uRotinas;

{ TFrmLancamentoCaixa }

procedure TFrmLancamentoCaixa.btnCancelarClick(Sender: TObject);
begin
 FrmLancamentoCaixa.ModalResult := mrCancel;
end;

procedure TFrmLancamentoCaixa.btnConfirmarClick(Sender: TObject);
begin
 if StrToFloat(lbTotal.Caption) <> StrToFloat(lbRecebido.Caption) then
 begin
   ShowMessage('O Valor Recebido n�o pode ser diferente do valor total.');
   Exit;
 end;
 if cbxCC.ItemIndex = 0 then
 begin
   ShowMessage('Selecione uma Conta Corrente');
   Exit;
 end;

 if not VerificaCaixaAberto(cbxCC.ItemIndex) then
 begin
   ShowMessage('Caixa Fechado, por favor selecione outra conta');
   Exit;
 end;

 try
  try
   with DM do
   begin
    cdsFin_Acerto.Open;
    cdsFin_Acerto.Append;
    cdsFin_AcertoDATA.Value       := Now;
    cdsFin_AcertoCODCAIXA.Value   := GetCodCaixa(cbxCC.ItemIndex);
    cdsFin_AcertoTPOPERACAO.Value := 2;
    cdsFin_AcertoSTATUS.Value     := 'CON';
    cdsFIN_PGTO.Open;
    cdsFIN_PGTO.Append;
    cdsFIN_PGTOCODACERTO.Value := cdsFin_AcertoCOD.Value;
    cdsFIN_PGTODINHEIRO.Value  := StrToFloat(edDinheiro.Text);
    cdsFIN_PGTOCHEQUE.Value    := StrToFloat(edCheque.Text);
    cdsFIN_PGTOCREDITO.Value   := StrToFloat(edCredito.Text);
    cdsFIN_PGTODEBITO.Value    := StrToFloat(edDebito.Text);
    cdsFIN_PGTOTPMOV.Value     := 'C';

    cdsFin_Acerto.ApplyUpdates(0);
    cdsFin_Acerto.Close;
    cdsFIN_PGTO.Close;
   end;
  finally
   ModalResult := mrOk;
  end;
 except
   DM.cdsFin_Acerto.CancelUpdates;
   ModalResult := mrCancel;
 end;
end;

procedure TFrmLancamentoCaixa.calculaValorRecebido;
var
 total: double;
begin
  total := StrToFloat(edDinheiro.Text)+StrToFloat(edCheque.Text)+StrToFloat(edDebito.Text)+ StrToFloat(edCredito.Text);
  lbRecebido.Caption := FormatFloat('##0.00',total);
end;

procedure TFrmLancamentoCaixa.edChequeKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = #13 then
  calculaValorRecebido
 else
  formataMoeda(Sender,Key);
end;

procedure TFrmLancamentoCaixa.edDinheiroKeyPress(Sender: TObject;
  var Key: Char);
begin
 if Key = #13 then
  calculaValorRecebido
 else
  formataMoeda(Sender,Key);
end;

procedure TFrmLancamentoCaixa.edCreditoKeyPress(Sender: TObject; var Key: Char);
begin
 if key = #13 then
  calculaValorRecebido
 else
  formataMoeda(Sender,Key);
end;

procedure TFrmLancamentoCaixa.edDebitoKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = #13 then
  calculaValorRecebido
 else
  formataMoeda(Sender,Key);
end;

procedure TFrmLancamentoCaixa.formataMoeda(Componente: TObject; var key: Char);
var
   str_valor  : String;
   dbl_valor  : double;
begin
    { verificando se estamos recebendo o TEdit realmente }
   IF Componente is TEdit THEN
   BEGIN
      { se tecla pressionada e' um numero, backspace ou del deixa passar }
      IF ( Key in ['0'..'9', #8, #9] ) THEN
      BEGIN
         { guarda valor do TEdit com que vamos trabalhar }
         str_valor := TEdit( Componente ).Text ;
         { verificando se nao esta vazio }
         IF str_valor = EmptyStr THEN str_valor := '0,00' ;
         { se valor numerico ja insere na string temporaria }
         IF Key in ['0'..'9'] THEN str_valor := Concat( str_valor, Key ) ;
         { retira pontos e virgulas se tiver! }
         str_valor := Trim( StringReplace( str_valor, '.', '', [rfReplaceAll, rfIgnoreCase] ) ) ;
         str_valor := Trim( StringReplace( str_valor, ',', '', [rfReplaceAll, rfIgnoreCase] ) ) ;
         {inserindo 2 casas decimais}
         dbl_valor := StrToFloat( str_valor ) ;
         dbl_valor := ( dbl_valor / 100 ) ;

         {reseta posicao do tedit}
         TEdit( Componente ).SelStart := Length( TEdit( Componente ).Text );
         {retornando valor tratado ao TEdit}
         TEdit( Componente ).Text := FormatFloat( '#####0.00', dbl_valor ) ;
      END;
      {se nao e' key relevante entao reseta}
      IF NOT( Key in [#8, #9] ) THEN key := #0;
   END;


end;

procedure TFrmLancamentoCaixa.FormShow(Sender: TObject);
begin
 getCC;
end;

procedure TFrmLancamentoCaixa.getCC;
begin
 with dm.QryTemp,SQL do
 begin
   Clear;
   Close;
   Add('select * from fin_contacorrente cc');
   Add('order by cc.cod');
   Open;
 end;

 cbxCC.Clear;
 cbxCC.Items.Add('Selecione uma Conta');
 DM.QryTemp.First;
 while not DM.QryTemp.Eof do
 begin
   cbxcc.Items.Add(DM.QryTemp.FieldByName('NOME').AsString);
   dm.QryTemp.Next;
 end;
 cbxCC.ItemIndex := 0;
end;

end.
