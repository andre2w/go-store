unit uFrmVendas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Data.DB, Datasnap.DBClient, Vcl.DBCtrls,
  Vcl.ActnList, Vcl.Mask, System.Actions;

type
  TFrmVendas = class(TForm)
    Panel1: TPanel;
    edtCodigo: TLabeledEdit;
    edtDescricao: TLabeledEdit;
    edtCategoria: TLabeledEdit;
    edtFabricante: TLabeledEdit;
    BtnBuscar: TBitBtn;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBGrid2: TDBGrid;
    btnLimpar: TBitBtn;
    btnFinalizar: TBitBtn;
    btnFechar: TBitBtn;
    DBGrid1: TDBGrid;
    dsConsultaProduto: TDataSource;
    cdsItems: TClientDataSet;
    dsItems: TDataSource;
    cdsItemsCODIGO: TIntegerField;
    cdsItemsDESCRICAO: TStringField;
    cdsItemsQTDE: TFloatField;
    cdsItemsPRECO: TFloatField;
    cdsItemsValorTotal: TFloatField;
    cdsItemsREFERENCIA: TStringField;
    ActionListPDV: TActionList;
    PDV_Buscar: TAction;
    PDV_Fechar: TAction;
    cdsItemsTotalItens: TAggregateField;
    cdsItemsSubTotal: TAggregateField;
    txtTotalItens: TDBText;
    txtSubTotal: TDBText;
    PDV_Finalizar: TAction;
    PDV_Limpaar: TAction;
    procedure BtnBuscarClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cdsItemsCalcFields(DataSet: TDataSet);
    procedure PDV_FecharExecute(Sender: TObject);
    procedure PDV_BuscarExecute(Sender: TObject);
    procedure PDV_FinalizarExecute(Sender: TObject);
    procedure PDV_LimpaarExecute(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure ConsultaProduto;
  public
    { Public declarations }
  end;

var
  FrmVendas: TFrmVendas;
  ValorTotalVenda: Extended;

implementation

{$R *.dfm}

uses uDM, uFrmItem, uFrmFinalizaVenda;

{ TFrmVendas }

procedure TFrmVendas.BtnBuscarClick(Sender: TObject);
begin
 ConsultaProduto;
end;

procedure TFrmVendas.btnFecharClick(Sender: TObject);
begin
 Close;
end;

procedure TFrmVendas.cdsItemsCalcFields(DataSet: TDataSet);
begin
  cdsItemsValorTotal.Value := cdsItemsQTDE.Value * cdsItemsPRECO.Value;
end;

procedure TFrmVendas.ConsultaProduto;
var
 S: TStringList;
begin
 S:= TStringList.Create;
 S.Add('SELECT p.produtoid,p.referencia, p.descricao, p.estoque, p.preco,');
 S.Add('       f.descricao, g.descricao');
 S.Add('from  produtos p, fabricante f, grupo g');
 S.Add('where p.codgrupo = g.cod');
 S.Add('  and p.codfabricante = f.cod');

 if EdtCodigo.Text <> EmptyStr then
  S.Add(' and P.REFERENCIA = '+ edtCodigo.Text);

 if edtDescricao.Text <> EmptyStr then
  S.Add(' and P.descricao like '+ QuotedStr(edtDescricao.Text+'%'));

 if edtCategoria.Text <> EmptyStr then
  S.Add(' and g.descricao like '+ QuotedStr(edtCategoria.Text+'%'));

 if edtFabricante.Text <> EmptyStr then
  S.Add(' and f.descricao like'+ QuotedStr(edtFabricante.Text+'%'));

  dm.cdsConsultaProduto.Close;
  dm.cdsConsultaProduto.CommandText := S.Text;
  dm.cdsConsultaProduto.Open;
end;

procedure TFrmVendas.DBGrid1DblClick(Sender: TObject);
begin
 FrmItem := TFrmItem.Create(nil);
 FrmItem.edtQuantidade.Text := '1';
 FrmItem.edtPreco.Text :=  FormatFloat('######0.00',DM.cdsConsultaProduto.FieldByName('PRECO').Value);
 if FrmItem.ShowModal = mrOk then
 begin
  if cdsItems.Locate('REFERENCIA;PRECO',VarArrayOf([dm.cdsConsultaProduto.FieldByName('REFERENCIA').AsString,FrmItem.edtPreco.Text]),[]) then
  begin
   cdsItems.Edit;
   cdsItemsQTDE.AsFloat := cdsItemsQTDE.Value + StrToFloat(FrmItem.edtQuantidade.Text);
   cdsItems.Post
  end
  else
  begin
   cdsItems.Append;
   cdsItemsCODIGO.Value := dm.cdsConsultaProduto.FieldByName('PRODUTOID').Value;
   cdsItemsREFERENCIA.AsString := dm.cdsConsultaProduto.FieldByName('REFERENCIA').AsString;
   cdsItemsDESCRICAO.AsString := dm.cdsConsultaProduto.FieldByName('DESCRICAO').AsString;
   cdsItemsPRECO.AsString := FrmItem.edtPreco.Text;
   cdsItemsQTDE.AsString := FrmItem.edtQuantidade.Text;
   cdsItems.Post;
  end;
 end;
 FreeAndNil(FrmItem);
end;

procedure TFrmVendas.FormCreate(Sender: TObject);
begin
 cdsItems.CreateDataSet;
end;


procedure TFrmVendas.PDV_BuscarExecute(Sender: TObject);
begin
 ConsultaProduto;
end;

procedure TFrmVendas.PDV_FecharExecute(Sender: TObject);
begin
 Dm.cdsConsultaProduto.Close;
 Close;
end;

procedure TFrmVendas.PDV_FinalizarExecute(Sender: TObject);
begin
 FrmFinalizaVenda := TFrmFinalizaVenda.Create(Self);
 ValorTotalVenda := cdsItemsSubTotal.Value;
 FrmFinalizaVenda.lblSubTotal.Caption := FormatFloat('#####0.00',ValorTotalVenda);
 FrmFinalizaVenda.ShowModal;
 FreeAndNil(FrmFinalizaVenda);
end;

procedure TFrmVendas.PDV_LimpaarExecute(Sender: TObject);
begin
 if MessageDlg('Deseja Limpar os Itens?',mtConfirmation,[mbYes,mbNo],0) = mrYes then
   cdsItems.EmptyDataSet;
end;

end.
