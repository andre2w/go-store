unit uUnitControl;

interface

uses
  Vcl.Forms;

procedure LiberaTela(AForm:TForm;ALibera:Boolean);

implementation

uses
  Vcl.DBCtrls, Vcl.StdCtrls, Edit2w;

procedure LiberaTela(AForm:TForm;ALibera:Boolean);
var
  I: Integer;
begin
  for I := 0 to Pred(AForm.ComponentCount) do
  begin
    if AForm.Components[i].ClassName = 'TDBEdit' then
    begin
      TDBEdit(AForm.Components[i]).Enabled := ALibera;
    end;
    if AForm.Components[i].ClassName = 'TDBLookupComboBox' then
    begin
      TDBLookupComboBox(AForm.Components[i]).Enabled := ALibera;
    end;
    if AForm.Components[i].ClassName = 'TEdit' then
    begin
      TEdit(AForm.Components[i]).Enabled := ALibera;
    end;
    if AForm.Components[i].ClassName = 'TEdit2w' then
    begin
      TEdit2w(AForm.Components[i]).Enabled := ALibera;
    end;
  end;

end;

end.
