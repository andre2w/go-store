//Tag 1 = Barra Expandida;


unit uFrmPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ImgList,
  Vcl.PlatformDefaultStyleActnCtrls, Vcl.ActnList, Vcl.ActnMan,
  Vcl.CustomizeDlg, Vcl.ActnCtrls, Vcl.ToolWin, Vcl.ActnMenus, Vcl.Menus,
  Vcl.ActnColorMaps, Vcl.Imaging.pngimage, Vcl.ExtCtrls, uFrmCliente,
  uFrmProduto, uFrmUsuario, uFrmPerfil, Vcl.Buttons, System.Actions,
  Vcl.StdCtrls;

type
  TModulo = (Cadastro,Seguranca,Financeiro,Vendas);
  TFrmPrincipal = class(TForm)
    Acoes: TActionManager;
    Imagens: TImageList;
    ac_ajuda_help: TAction;
    ac_ajuda_sobre: TAction;
    ac_consulta_MovimentoDiario: TAction;
    ac_vendas_pdv: TAction;
    ActionMainMenuBar1: TActionMainMenuBar;
    ac_relatorios_listaClientes: TAction;
    ac_relatorios_VendasDia: TAction;
    ac_seguranca_logoff: TAction;
    ac_seguranca_perfil: TAction;
    ac_cadastro_clientes: TAction;
    ac_cadastro_produtos: TAction;
    ac_cadastro_usuarios: TAction;
    ac_cadastro_sair: TAction;
    CustomizeDlg1: TCustomizeDlg;
    PopupMenu1: TPopupMenu;
    Customizar1: TMenuItem;
    XPColorMap1: TXPColorMap;
    Image1: TImage;
    ac_seguranca_alteraSenha: TAction;
    ac_seguranca_ControleDeAcesso: TAction;
    ac_cadastro_Fabricante: TAction;
    ac_cadastro_grupo: TAction;
    BarraLateral: TPanel;
    btnCadastro: TSpeedButton;
    btnSeguranca: TSpeedButton;
    btnFinancas: TSpeedButton;
    pnCadastros: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    pnSeguranca: TPanel;
    btnPerfil: TSpeedButton;
    btnMudarSenha: TSpeedButton;
    SpeedButton5: TSpeedButton;
    ac_cadastro_MovEstoque: TAction;
    pnFinanceiro: TPanel;
    btnContas: TSpeedButton;
    ac_financas_contas: TAction;
    ac_financas_operacoes: TAction;
    pnVendas: TPanel;
    ac_vendas_consignado: TAction;
    btnVendas: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    ac_cadastro_cc: TAction;
    btnAcerto: TSpeedButton;
    ac_financas_caixa: TAction;
    btnCaixa: TSpeedButton;
    procedure Customizar1Click(Sender: TObject);
    procedure ac_cadastro_clientesExecute(Sender: TObject);
    procedure ac_cadastro_produtosExecute(Sender: TObject);
    procedure ac_cadastro_usuariosExecute(Sender: TObject);
    procedure ac_seguranca_perfilExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ac_seguranca_logoffExecute(Sender: TObject);
    procedure ac_seguranca_alteraSenhaExecute(Sender: TObject);
    procedure ac_seguranca_ControleDeAcessoExecute(Sender: TObject);
    procedure ac_vendas_pdvExecute(Sender: TObject);
    procedure ac_cadastro_FabricanteExecute(Sender: TObject);
    procedure ac_cadastro_grupoExecute(Sender: TObject);
    procedure BarraLateralClick(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCadastroClick(Sender: TObject);
    procedure btnSegurancaClick(Sender: TObject);
    procedure BarraLateralResize(Sender: TObject);
    procedure ac_cadastro_MovEstoqueExecute(Sender: TObject);
    procedure ac_financas_contasExecute(Sender: TObject);
    procedure btnFinancasClick(Sender: TObject);
    procedure btnRelatoriosClick(Sender: TObject);
    procedure ac_vendas_consignadoExecute(Sender: TObject);
    procedure btnVendasClick(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure ac_cadastro_ccExecute(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure ac_financas_operacoesExecute(Sender: TObject);
    procedure ac_financas_caixaExecute(Sender: TObject);
  private
    { Private declarations }
    procedure ExpandirBarra;
    procedure EncolherBarra;
    procedure GetModulo(APanel:TModulo);
    procedure EscondePanel(APainel:TModulo);
  public
    { Public declarations }
  end;

var
  FrmPrincipal: TFrmPrincipal;
  expandida: Boolean;

implementation

{$R *.dfm}

uses uFrmLogin, uFrmAlteraSenha, uFrmAcesso, uUsuario, uFrmVendas,
  uFrmFabricante, uFrmGrupo, Dbxcommon, uFrmPesqVendas, uFrmMovEstoque,
  uFrmMovimentaEstoque, uFrmContas, uFrmRelatorio, uFrmPesqConsignado, uFrmCC,
  uFrmMovCaixa, uFrmAcerto;

procedure TFrmPrincipal.ac_cadastro_ccExecute(Sender: TObject);
begin
 FrmCC := TFrmCC.Create(Self);
 FrmCC.ShowModal;
 FreeAndNil(FrmCC);
end;

procedure TFrmPrincipal.ac_cadastro_clientesExecute(Sender: TObject);
begin
 FrmCliente := TFrmCliente.Create(Self);
 FrmCliente.ShowModal;
 FreeAndNil(FrmCliente);
end;

procedure TFrmPrincipal.ac_cadastro_FabricanteExecute(Sender: TObject);
begin
 FrmFabricante := TFrmFabricante.Create(Self);
 FrmFabricante.ShowModal;
 FreeAndNil(FrmFabricante);
end;

procedure TFrmPrincipal.ac_cadastro_grupoExecute(Sender: TObject);
begin
 FrmGrupo := TFrmGrupo.Create(Self);
 FrmGrupo.ShowModal;
 FreeAndNil(FrmGrupo);
end;

procedure TFrmPrincipal.ac_cadastro_MovEstoqueExecute(Sender: TObject);
begin
 FrmMovimentaEstoque := TFrmMovimentaEstoque.Create(Self);
 FrmMovimentaEstoque.ShowModal;
 FreeAndNil(FrmMovimentaEstoque);
end;

procedure TFrmPrincipal.ac_cadastro_produtosExecute(Sender: TObject);
begin
 FrmProduto := TFrmProduto.Create(Self);
 FrmProduto.ShowModal;
 FreeAndNil(FrmProduto);
end;

procedure TFrmPrincipal.ac_cadastro_usuariosExecute(Sender: TObject);
begin
 FrmUsuario := TFrmUsuario.Create(Self);
 FrmUsuario.ShowModal;
 FreeAndNil(FrmUsuario);
end;

procedure TFrmPrincipal.ac_financas_caixaExecute(Sender: TObject);
begin
 FrmMovCaixa := TFrmMovCaixa.Create(Self);
 FrmMovCaixa.ShowModal;
 FreeAndNil(FrmMovCaixa);
end;

procedure TFrmPrincipal.ac_financas_contasExecute(Sender: TObject);
begin
 FrmContas := TFrmContas.Create(Self);
 FrmContas.ShowModal;
 FreeAndNil(FrmContas);
end;

procedure TFrmPrincipal.ac_financas_operacoesExecute(Sender: TObject);
begin
 FrmAcerto := TFrmAcerto.Create(Self);
 FrmAcerto.ShowModal;
 FreeAndNil(FrmAcerto);
end;

procedure TFrmPrincipal.ac_seguranca_alteraSenhaExecute(Sender: TObject);
begin
 FrmAlteraSenha := TFrmAlteraSenha.Create(Self);
 FrmAlteraSenha.ShowModal;
 FreeAndNil(FrmAlteraSenha);
end;

procedure TFrmPrincipal.ac_seguranca_ControleDeAcessoExecute(Sender: TObject);
begin
  FrmAcesso := TFrmAcesso.Create(Self);
  FrmAcesso.ShowModal;
  FreeAndNil(FrmAcesso);
end;

procedure TFrmPrincipal.ac_seguranca_logoffExecute(Sender: TObject);
begin
 FormCreate(Self);
end;

procedure TFrmPrincipal.ac_seguranca_perfilExecute(Sender: TObject);
begin
 FrmPerfil := TFrmPerfil.Create(Self,Acoes);
 FrmPerfil.ShowModal;
 FreeAndNil(FrmPerfil);
end;

procedure TFrmPrincipal.ac_vendas_consignadoExecute(Sender: TObject);
begin
 FrmPesqConsignado := TFrmPesqConsignado.Create(Self);
 FrmPesqConsignado.ShowModal;
 FreeAndNil(FrmPesqConsignado);
end;

procedure TFrmPrincipal.ac_vendas_pdvExecute(Sender: TObject);
begin
 FrmPesqVendas := TFrmPesqVendas.Create(Self);
 FrmPesqVendas.ShowModal;
 FreeAndNil(FrmPesqVendas);
end;

procedure TFrmPrincipal.BarraLateralClick(Sender: TObject);
begin
 if expandida then
 begin
   EncolherBarra
 end
 else
 begin
  ExpandirBarra
 end;

end;

procedure TFrmPrincipal.BarraLateralResize(Sender: TObject);
begin
 if not pnCadastros.Visible then
 begin
  ac_cadastro_clientes.ShortCut   := 0;
  ac_cadastro_produtos.ShortCut   := 0;
  ac_cadastro_usuarios.ShortCut   := 0;
  ac_cadastro_MovEstoque.ShortCut := 0;
 end;

 if not pnSeguranca.Visible then
 begin
  ac_seguranca_perfil.ShortCut           := 0;
  ac_seguranca_alteraSenha.ShortCut      := 0;
  ac_seguranca_ControleDeAcesso.ShortCut := 0;
 end;

 if not pnFinanceiro.Visible then
 begin
   ac_financas_contas.ShortCut    := 0;
   ac_financas_operacoes.ShortCut := 0;
   ac_financas_caixa.ShortCut     := 0;
 end;

 if not pnVendas.Visible then
 begin
   ac_vendas_pdv.ShortCut        := 0;
   ac_vendas_consignado.ShortCut := 0;
 end;

end;

procedure TFrmPrincipal.BitBtn1Click(Sender: TObject);
begin
 FrmMovCaixa := TFrmMovCaixa.Create(Self);
 FrmMovCaixa.ShowModal;
 FreeAndNil(FrmMovCaixa);
end;

procedure TFrmPrincipal.btnCadastroClick(Sender: TObject);
begin
 GetModulo(Cadastro);
end;

procedure TFrmPrincipal.btnFinancasClick(Sender: TObject);
begin
 GetModulo(Financeiro);
end;

procedure TFrmPrincipal.btnRelatoriosClick(Sender: TObject);
begin
 FrmRelatorios := TFrmRelatorios.Create(nil);
 FrmRelatorios.ShowModal;
 FreeAndNil(FrmRelatorios);
end;

procedure TFrmPrincipal.btnSegurancaClick(Sender: TObject);
begin
 GetModulo(Seguranca);
end;

procedure TFrmPrincipal.btnVendasClick(Sender: TObject);
begin
 GetModulo(Vendas);
end;

procedure TFrmPrincipal.Customizar1Click(Sender: TObject);
begin
 CustomizeDlg1.Show;
end;

procedure TFrmPrincipal.EncolherBarra;
begin
  BarraLateral.Width := 33;
  BarraLateral.Caption := '>>';

  btnCadastro.Visible  := False;
  btnVendas.Visible       := False;
  btnSeguranca.Visible := False;
  btnFinancas.Visible  := False;
  btnVendas.Visible    := False;

  expandida := False;
end;

procedure TFrmPrincipal.EscondePanel(APainel: TModulo);
begin
 case APainel of
   Cadastro:  begin
               pnVendas.Visible     := False;
               pnSeguranca.Visible  := False;
               pnFinanceiro.Visible := False;
              end;
   Seguranca: begin
               pnCadastros.Visible := False;
               pnVendas.Visible    := False;
               pnFinanceiro.Visible:= False;
              end;
   Financeiro:begin
               pnCadastros.Visible := False;
               pnVendas.Visible    := False;
               pnSeguranca.Visible := False;
              end;
   Vendas:    begin
               pnCadastros.Visible  := False;
               pnSeguranca.Visible  := False;
               pnFinanceiro.Visible := False;
              end;
 end;
end;

procedure TFrmPrincipal.ExpandirBarra;
begin
  BarraLateral.Width   := 137;
  BarraLateral.Caption := '';

  btnCadastro.Visible  := True;
  btnVendas.Visible       := True;
  btnSeguranca.Visible := True;
  btnFinancas.Visible  := True;
  btnVendas.Visible    := True;

  expandida := True;
end;

procedure TFrmPrincipal.FormClick(Sender: TObject);
begin
 if expandida then
  EncolherBarra
end;

procedure TFrmPrincipal.FormCreate(Sender: TObject);
begin
 FrmLogin := TFrmLogin.Create(Self);
 if FrmLogin.ShowModal <> mrOK then
 begin
   MessageDlg('Login ou Senha Invalido!', mtInformation,[mbOK],0);
   Application.Terminate;
 end;
 FreeAndNil(FrmLogin);

end;

procedure TFrmPrincipal.FormShow(Sender: TObject);
begin
 ExpandirBarra;
end;

procedure TFrmPrincipal.GetModulo(APanel: TModulo);
begin
 EscondePanel(APanel);
 case APanel of
  Cadastro:   begin
               pnCadastros.Top := 32;
               pnCadastros.Visible := True;
               ac_cadastro_clientes.ShortCut := VK_F1;
               ac_cadastro_produtos.ShortCut := VK_F2;
               ac_cadastro_usuarios.ShortCut := VK_F3;
               ac_cadastro_MovEstoque.ShortCut := VK_F4;
              end;
  Seguranca:  begin
               pnSeguranca.Top := 32;
               pnSeguranca.Visible := True;
               ac_seguranca_perfil.ShortCut := VK_F1;
               ac_seguranca_alteraSenha.ShortCut := VK_F2;
              end;
  Financeiro: begin
               pnFinanceiro.Top := 32;
               pnFinanceiro.Visible := True;
               ac_financas_contas.ShortCut := VK_F1;
               ac_financas_operacoes.ShortCut := VK_F2;
               ac_financas_caixa.ShortCut := VK_F3;
              end;
  Vendas:     begin
               pnVendas.Top := 32;
               pnVendas.Visible := True;
               ac_vendas_pdv.ShortCut := VK_F1;
               ac_vendas_consignado.ShortCut := VK_F2;
              end;

 end;
 EncolherBarra;
end;

procedure TFrmPrincipal.SpeedButton9Click(Sender: TObject);
begin
 FrmRelatorios := TFrmRelatorios.Create(Self);
 FrmRelatorios.ShowModal;
 FreeAndNil(FrmRelatorios);
end;

end.
