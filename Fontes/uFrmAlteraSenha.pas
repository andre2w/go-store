unit uFrmAlteraSenha;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls;

type
  TFrmAlteraSenha = class(TForm)
    edtSenhaAtual: TLabeledEdit;
    edtNovaSenha: TLabeledEdit;
    btnOK: TBitBtn;
    btnCancelar: TBitBtn;
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmAlteraSenha: TFrmAlteraSenha;

implementation

{$R *.dfm}

uses uUsuario;

procedure TFrmAlteraSenha.btnCancelarClick(Sender: TObject);
begin
 Close;
end;

procedure TFrmAlteraSenha.btnOKClick(Sender: TObject);
begin
 if TUsuario.GetInstance.ChangePasword(edtSenhaAtual.Text,edtNovaSenha.Text) then
 begin
   MessageDlg('Senha Alterada com sucesso!',mtInformation,[mbOK],0);
   Close;
 end
 else
 begin
   MessageDlg('N�o foi poss�vel alterar a senha!', mtError,[mbOK],0);
 end;
end;

end.
