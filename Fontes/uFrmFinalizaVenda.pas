unit uFrmFinalizaVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.DBCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, Data.DB, Vcl.ActnList,
  System.Actions;

type
  TFrmFinalizaVenda = class(TForm)
    edtCliente: TLabeledEdit;
    PageControl1: TPageControl;
    tbPesquisa: TTabSheet;
    tbCliente: TTabSheet;
    Label1: TLabel;
    BtnBuscar: TBitBtn;
    DBGrid1: TDBGrid;
    btnFinalizar: TBitBtn;
    dsPesquisaCliente: TDataSource;
    ActionList1: TActionList;
    finalizaVenda_busca: TAction;
    finalizaVenda_Finalizar: TAction;
    edtClienteID: TLabeledEdit;
    edtNome: TLabeledEdit;
    edtBairro: TLabeledEdit;
    edtEndereco: TLabeledEdit;
    edtTelefone: TLabeledEdit;
    lblSubTotal: TLabel;
    btnLimpar: TBitBtn;
    finalizaVenda_Fechar: TAction;
    procedure edtClienteKeyPress(Sender: TObject; var Key: Char);
    procedure BtnBuscarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure finalizaVenda_buscaExecute(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure finalizaVenda_FecharExecute(Sender: TObject);
    procedure finalizaVenda_FinalizarExecute(Sender: TObject);

  private
    procedure BuscaCliente;
    procedure atualizaSituacaoVenda(ACodVenda: Integer);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmFinalizaVenda: TFrmFinalizaVenda;
  UsuID: integer;

implementation

{$R *.dfm}

uses uDM, uFrmVendas, uUsuario, uFrmPreFinanceiro, uRotinas,
  uFrmLancamentoCaixa;

procedure TFrmFinalizaVenda.atualizaSituacaoVenda(ACodVenda: Integer);
begin
 with DM.QryTemp,SQL do
 begin
   Clear;
   Close;
   Add('update vendas v           ');
   Add('set v.situacao = ''LAN''  ');
   Add('where v.vendaid = ' + IntToStr(ACodVenda));
   ExecSQL(True);
 end;
end;

procedure TFrmFinalizaVenda.BtnBuscarClick(Sender: TObject);
begin
 if edtCliente.Text <> EmptyStr then
 begin
   BuscaCliente;
 end;
end;

procedure TFrmFinalizaVenda.BuscaCliente;
var
 S: TStringList;
begin
  DM.cdsPesquisaCliente.Close;
  Dm.cdsPesquisaCliente.CommandText := '';
  S := TStringList.Create;
  S.Add(' Select c.clienteid, c.nome, c.endereco, c.bairro, c.telefone ');
  S.Add(' from clientes c');
  S.Add(' where c.nome like'+ QuotedStr(edtCliente.Text+'%'));
  Dm.cdsPesquisaCliente.CommandText := S.Text;
  DM.cdsPesquisaCliente.Open;
  PageControl1.ActivePage := tbPesquisa;

end;

procedure TFrmFinalizaVenda.DBGrid1DblClick(Sender: TObject);
begin
 edtClienteID.Text := DM.cdsPesquisaCliente.FieldByName('CLIENTEID').AsString;
 edtNome.Text      := DM.cdsPesquisaCliente.FieldByName('NOME').AsString;
 edtEndereco.Text  := DM.cdsPesquisaCliente.FieldByName('ENDERECO').AsString;
 edtBairro.Text    := DM.cdsPesquisaCliente.FieldByName('BAIRRO').AsString;
 edtTelefone.Text  := DM.cdsPesquisaCliente.FieldByName('TELEFONE').AsString;
 PageControl1.ActivePage := tbCliente;
end;

procedure TFrmFinalizaVenda.edtClienteKeyPress(Sender: TObject;
  var Key: Char);
begin
 if key = #13 then
 begin
   if edtCliente.Text = EmptyStr then
   begin
     ShowMessage('Selecione um cliente');
   end
   else
   begin
    BuscaCliente
   end;
 end;
end;

procedure TFrmFinalizaVenda.finalizaVenda_buscaExecute(Sender: TObject);
begin
 BuscaCliente;
end;

procedure TFrmFinalizaVenda.finalizaVenda_FecharExecute(Sender: TObject);
begin
 Close;
end;

procedure TFrmFinalizaVenda.finalizaVenda_FinalizarExecute(Sender: TObject);
begin
  if edtClienteID.Text = EmptyStr then
  begin
    ShowMessage('Voc� precisa selecionar um Cliente');
    Exit;
  end;

  with DM do
  begin
    if not cdsVendas.Active then cdsVendas.Open;
    cdsVendas.Append;
    cdsVendasCLIENTEID.Value := StrToInt(edtClienteID.Text);
    cdsVendasUSUARIOID.Value := UsuID;
    cdsVendasDATA.Value      := Date;
    cdsVendasCODTPMOVIMENTO.Value  := 1;
    cdsVendasSITUACAO.Value  := 'ABE';
    FrmVendas.cdsItems.First;
    while not FrmVendas.cdsItems.Eof do
    begin
      cdsVendaItens.Append;
      cdsVendaItens.FieldByName('PRODUTOID').Value  := FrmVendas.cdsItemsCODIGO.Value;
      cdsVendaItens.FieldByName('QTDE').Value       := FrmVendas.cdsItemsQTDE.Value;
      cdsVendaItens.FieldByName('PRECO').Value      := FrmVendas.cdsItemsPRECO.Value;
      cdsVendaItens.Post;
      FrmVendas.cdsItems.Next;
    end;
    cdsVendas.ApplyUpdates(0);

    if MessageDlg('Deseja lan�ar essa venda no caixa?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
     FrmLancamentoCaixa := TFrmLancamentoCaixa.Create(nil);
     FrmLancamentoCaixa.lbTotal.Caption := FormatFloat('##0.00',StrToFloat(FrmVendas.cdsItemsSubTotal.AsString)); // POG PARA FORMATAR O VALOR CORRETAMENTE
     FrmLancamentoCaixa.IDCliente   := StrToInt(edtClienteID.Text);
     FrmLancamentoCaixa.NomeCliente := edtNome.Text;
     FrmLancamentoCaixa.IDVenda     := cdsVendasVENDAID.Value;
     if FrmLancamentoCaixa.ShowModal = mrOk then
     begin
       ShowMessage('Caixa Lan�ado com Sucesso!');
       FreeAndNil(FrmLancamentoCaixa);
       atualizaSituacaoVenda(cdsVendasVENDAID.Value);
     end
     else
     begin
       FreeAndNil(FrmLancamentoCaixa);
     end;
    end;
    GravarLog(cdsVendasVENDAID.Value,'PDV');
    cdsVendas.Close;
    FrmVendas.cdsItems.EmptyDataSet;
    Close;
  end;
end;

procedure TFrmFinalizaVenda.FormShow(Sender: TObject);
begin
 PageControl1.ActivePage := tbPesquisa;
 UsuID := TUsuario.GetInstance.ID;
end;

end.
