// 1- pagar 2- receber

unit uFrmAcerto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids,
  Data.DB, Datasnap.DBClient, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.Buttons,
  System.Actions, Vcl.ActnList, Data.FMTBcd, Datasnap.Provider, Data.SqlExpr,
  Vcl.Mask, Vcl.DBCtrls, Vcl.Menus;

type
  TTipoMov = (Pagar,Receber);
  TFrmAcerto = class(TForm)
    PageControl1: TPageControl;
    tbPesquisa: TTabSheet;
    tbAcerto: TTabSheet;
    pnAcertoBottom: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    dbgAcerto: TDBGrid;
    cbxCC: TComboBox;
    edtVLPagoAcerto: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtDinheiro: TEdit;
    edtCheque: TEdit;
    edtCredito: TEdit;
    edtDebito: TEdit;
    Label4: TLabel;
    lbCheque: TLabel;
    Label5: TLabel;
    lbDebito: TLabel;
    tbTitulos: TTabSheet;
    Panel4: TPanel;
    Panel5: TPanel;
    dbgTitulos: TDBGrid;
    cbxTitulos: TComboBox;
    edtPesquisaTitulos: TEdit;
    cbxDataTitulos: TComboBox;
    dtTitulosI: TDateTimePicker;
    dtTitulosF: TDateTimePicker;
    Label6: TLabel;
    btnPesquisarTitulos: TBitBtn;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    ActionList1: TActionList;
    acPesquisar: TAction;
    sdsTitulos: TSQLDataSet;
    dspTitulos: TDataSetProvider;
    cdsTitulos: TClientDataSet;
    dsTitulos: TDataSource;
    sdsTitulosCOD: TIntegerField;
    sdsTitulosPESSOA: TStringField;
    sdsTitulosDOCPARCELA: TStringField;
    sdsTitulosTPMOVIMENTO: TIntegerField;
    sdsTitulosVALOR: TSingleField;
    sdsTitulosVENCIMENTO: TDateField;
    sdsTitulosSTATUS: TStringField;
    sdsTitulosNOME: TStringField;
    sdsTitulosNUMDOCUMENTO: TStringField;
    sdsTitulosPARCELA: TIntegerField;
    sdsTitulosCODPARCELA: TSmallintField;
    sdsTitulosDATAEMISSAO: TDateField;
    sdsTitulosMK: TStringField;
    sdsTitulosTIPOMOV: TStringField;
    cdsTitulosCOD: TIntegerField;
    cdsTitulosPESSOA: TStringField;
    cdsTitulosDOCPARCELA: TStringField;
    cdsTitulosTPMOVIMENTO: TIntegerField;
    cdsTitulosVALOR: TSingleField;
    cdsTitulosVENCIMENTO: TDateField;
    cdsTitulosSTATUS: TStringField;
    cdsTitulosNOME: TStringField;
    cdsTitulosNUMDOCUMENTO: TStringField;
    cdsTitulosPARCELA: TIntegerField;
    cdsTitulosCODPARCELA: TSmallintField;
    cdsTitulosDATAEMISSAO: TDateField;
    cdsTitulosMK: TStringField;
    cdsTitulosTIPOMOV: TStringField;
    btnSairTitulo: TBitBtn;
    acSair: TAction;
    btnConfirmarTitulos: TBitBtn;
    cdsAcerto: TClientDataSet;
    dsAcerto: TDataSource;
    cdsAcertoPESSOA: TStringField;
    cdsAcertoDOCUMENTO: TStringField;
    cdsAcertoVENCIMENTO: TDateTimeField;
    cdsAcertoVALOR: TFloatField;
    cdsAcertoTIPO: TStringField;
    cdsAcertoEMISSAO: TDateTimeField;
    acConfirmar: TAction;
    cdsAcertoCOD: TIntegerField;
    cdsAcertoCODPARC: TIntegerField;
    edtVLTotalAcerto: TEdit;
    BitBtn2: TBitBtn;
    btnConfirmarAcerto: TBitBtn;
    btnReceber: TBitBtn;
    btnPagar: TBitBtn;
    BitBtn3: TBitBtn;
    Panel1: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    cbxTpPesquisa: TComboBox;
    edtPesquisa: TEdit;
    dtPesquisaI: TDateTimePicker;
    dtPesquisaF: TDateTimePicker;
    btnPesquisar: TBitBtn;
    sdsPesquisa: TSQLDataSet;
    dspPesquisa: TDataSetProvider;
    cdsPesquisa: TClientDataSet;
    dsPesquisa: TDataSource;
    dbgPesquisa: TDBGrid;
    sdsPesquisaCOD: TIntegerField;
    sdsPesquisaDATA: TDateField;
    sdsPesquisaCODCAIXA: TIntegerField;
    sdsPesquisaTPOPERACAO: TStringField;
    sdsPesquisaSTATUS: TStringField;
    sdsPesquisaNOME: TStringField;
    sdsPesquisaCODCC: TIntegerField;
    sdsPesquisaCODPGTO: TIntegerField;
    sdsPesquisaDINHEIRO: TSingleField;
    sdsPesquisaCHEQUE: TSingleField;
    sdsPesquisaCREDITO: TSingleField;
    sdsPesquisaDEBITO: TSingleField;
    sdsPesquisaTOTAL: TFloatField;
    cdsPesquisaCOD: TIntegerField;
    cdsPesquisaDATA: TDateField;
    cdsPesquisaCODCAIXA: TIntegerField;
    cdsPesquisaTPOPERACAO: TStringField;
    cdsPesquisaSTATUS: TStringField;
    cdsPesquisaNOME: TStringField;
    cdsPesquisaCODCC: TIntegerField;
    cdsPesquisaCODPGTO: TIntegerField;
    cdsPesquisaDINHEIRO: TSingleField;
    cdsPesquisaCHEQUE: TSingleField;
    cdsPesquisaCREDITO: TSingleField;
    cdsPesquisaDEBITO: TSingleField;
    cdsPesquisaTOTAL: TFloatField;
    btnVisualizarPesq: TBitBtn;
    acVisualizar: TAction;
    cdsAcertoCODPESSOA: TIntegerField;
    sdsTitulosCODPESSOA: TIntegerField;
    cdsTitulosCODPESSOA: TIntegerField;
    Label11: TLabel;
    PopupMenu1: TPopupMenu;
    btnEstornar: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure acPesquisarExecute(Sender: TObject);
    procedure dbgTitulosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgTitulosDblClick(Sender: TObject);
    procedure acSairExecute(Sender: TObject);
    procedure tbTitulosShow(Sender: TObject);
    procedure acConfirmarExecute(Sender: TObject);
    procedure edtDinheiroKeyPress(Sender: TObject; var Key: Char);
    procedure edtChequeKeyPress(Sender: TObject; var Key: Char);
    procedure edtCreditoKeyPress(Sender: TObject; var Key: Char);
    procedure edtDebitoKeyPress(Sender: TObject; var Key: Char);
    procedure edtDebitoClick(Sender: TObject);
    procedure edtDinheiroClick(Sender: TObject);
    procedure edtDinheiroExit(Sender: TObject);
    procedure edtChequeExit(Sender: TObject);
    procedure edtCreditoExit(Sender: TObject);
    procedure edtDebitoExit(Sender: TObject);
    procedure btnReceberClick(Sender: TObject);
    procedure btnPagarClick(Sender: TObject);
    procedure acVisualizarExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnEstornarClick(Sender: TObject);
  private
    { Private declarations }
    procedure getCC;
    procedure pesquisaTitulos;
    procedure limpaTela(ATela:string);
    procedure carregaTitulos;
    procedure FormataMoeda(Componente : TObject; var Key: Char);
    procedure calculaValorPago;
    procedure realizaAcerto(ATpAcerto:TTipoMov);
    procedure AtualizaTitulos(ACodAcerto:Integer);
    procedure pesquisaAcertos;
    procedure visualizaTitulos(ACodAcerto:Integer);
    procedure mudarTela(ATela: string);
  public
    { Public declarations }
  end;

var
  FrmAcerto: TFrmAcerto;
  TpMov : TTipoMov;
  Visualizando: Boolean;

implementation

{$R *.dfm}

uses uDM, uRotinas;

{ TFrmAcerto }

procedure TFrmAcerto.acConfirmarExecute(Sender: TObject);
begin
 if PageControl1.ActivePage = tbTitulos then
 begin
  carregaTitulos;
  PageControl1.ActivePage := tbAcerto;
  limpaTela('tbTitulos');
 end
 else if PageControl1.ActivePage = tbAcerto then
 begin
  realizaAcerto(TpMov);
  limpaTela('tbAcerto');
  mudarTela('tbPesquisa');
 end
end;

procedure TFrmAcerto.acPesquisarExecute(Sender: TObject);
begin
 if PageControl1.ActivePage = tbTitulos then
   pesquisaTitulos
 else if PageControl1.ActivePage = tbPesquisa then
 begin
   pesquisaAcertos;
 end;

end;

procedure TFrmAcerto.acSairExecute(Sender: TObject);
begin
 if PageControl1.ActivePage = tbTitulos then
 begin
   limpaTela('tbTitulos');
   mudarTela('tbPesquisa');
 end
 else if PageControl1.ActivePage = tbAcerto then
 begin
  limpaTela('tbAcerto');
  mudarTela('tbPesquisa');
 end
 else if PageControl1.ActivePage = tbPesquisa then
  Close;
end;

procedure TFrmAcerto.acVisualizarExecute(Sender: TObject);
begin
 if cdsPesquisaSTATUS.Value = 'EST' then
 begin
   ShowMessage('N�o � possivel visualizar este acerto');
   Exit;
 end;

 Visualizando := True;
 visualizaTitulos(cdsPesquisaCOD.Value);
 mudarTela('tbAcerto');
end;

procedure TFrmAcerto.AtualizaTitulos(ACodAcerto:Integer);
var
 Qry : TSQLQuery;
begin
 try
  Qry := TSQLQuery.Create(nil);
  Qry.SQLConnection := DM.DBAcesso;
  cdsAcerto.First;
  while not cdsAcerto.Eof do
  begin
   with Qry,SQL do
   begin
     Clear;
     Close;
     Add(' update fin_docparcela p');
     Add(' set p.status = ''QUI'', ');
     Add(' p.codacerto = ' + IntToStr(ACodAcerto) );
     Add(' WHERE p.codparcela = '+ cdsAcertoCODPARC.AsString);
     ExecSQL(True);
   end;
   cdsAcerto.Next;
  end;
 finally
  FreeAndNil(Qry);
 end;
end;

procedure TFrmAcerto.btnEstornarClick(Sender: TObject);
var
 Qry:TSQLQuery;
begin
 try
  try
   Qry := TSQLQuery.Create(nil);
   Qry.SQLConnection := DM.DBAcesso;


   with Qry,SQL do
   begin
    Clear;
    Close;
    Add('  select cx.status from fin_caixa cx ');
    Add('  where cx.cod = '+ cdsPesquisaCODCAIXA.AsString);
    Open;
   end;
   if qry.FieldByName('STATUS').AsString = 'FEC' then
   BEGIN
     ShowMessage('N�o � possivel estornar conta para caixa fehcado');
     Exit;
   END;

   with Qry,SQL do
   begin
    Clear;
    Close;
    Add(' update fin_acerto a ');
    Add(' set a.status = ''EST'' ');
    Add(' where a.cod = '+ cdsPesquisaCOD.AsString);
    ExecSQL(True);
   end;

   with Qry,SQL do
   begin
     Clear;
     Close;
     Add(' update fin_docparcela p ');
     Add(' set p.status = ''ABE''  ');
     Add(' where p.codacerto = '+ cdsPesquisaCOD.AsString);
     ExecSQL(True);
   end;

  finally
    FreeAndNil(Qry);
  end;

 except
  ShowMessage('Falha ao estornar titulos!');
 end;
end;

procedure TFrmAcerto.btnPagarClick(Sender: TObject);
begin
 TpMov := Pagar;
 mudarTela('tbTitulos');
 btnPesquisarTitulos.Click;
end;

procedure TFrmAcerto.btnReceberClick(Sender: TObject);
begin
 TpMov := Receber;
 mudarTela('tbTitulos');
 btnPesquisarTitulos.Click;
end;

procedure TFrmAcerto.calculaValorPago;
var
 troco, vlpago, dinheiro, cheque, credito, debito : Double;
begin
 vlpago   := 0;
 dinheiro := StrToFloat(edtDinheiro.Text);
 cheque   := StrToFloat(edtCheque.Text);
 credito  := StrToFloat(edtCredito.Text);
 debito   := StrToFloat(edtDebito.Text);
 vlpago   := dinheiro + cheque + credito + debito;
 edtVLPagoAcerto.Text := FormatFloat('##0.00',vlpago);

end;

procedure TFrmAcerto.carregaTitulos;
var
 totaltitulos : Double;
begin
 totaltitulos := 0;
 cdsAcerto.Open;
 cdsAcerto.EmptyDataSet;
 cdsTitulos.First;
 while not cdsTitulos.Eof do
 begin
  if cdsTitulosMK.Value = 'X' then
  begin
   cdsAcerto.Append;
   cdsAcertoPESSOA.Value     := cdsTitulosPESSOA.Value;
   cdsAcertoCODPESSOA.Value  := cdsTitulosCODPESSOA.Value;
   cdsAcertoDOCUMENTO.Value  := cdsTitulosNUMDOCUMENTO.Value;
   cdsAcertoVENCIMENTO.Value := cdsTitulosVENCIMENTO.Value;
   cdsAcertoVALOR.Value      := cdsTitulosVALOR.Value;
   totaltitulos := totaltitulos + cdsAcertoVALOR.Value;
   cdsAcertoTIPO.Value       := cdsTitulosTIPOMOV.Value;
   cdsAcertoEMISSAO.Value    := cdsTitulosDATAEMISSAO.Value;
   cdsAcertoCOD.Value        := cdsTitulosCOD.Value;
   cdsAcertoCODPARC.Value    := cdsTitulosCODPARCELA.Value;
   cdsAcerto.Post;
   cdsTitulos.Next;
  end
  else
  begin
    cdsTitulos.Next;
  end;
 end;
 edtVLTotalAcerto.Text := FormatFloat('###0.00',totaltitulos);
end;

procedure TFrmAcerto.dbgTitulosDblClick(Sender: TObject);
begin
 if cdsTitulosMK.Value = '' then
 begin
  cdsTitulos.Edit;
  cdsTitulosMK.Value := 'X';
  cdsTitulos.Post;
 end
 else
 begin
  cdsTitulos.Edit;
  cdsTitulosMK.Value := ' ';
  cdsTitulos.Post;
 end;
end;

procedure TFrmAcerto.dbgTitulosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
 if cdsTitulosVENCIMENTO.Value < Now then
 begin
   dbgTitulos.Canvas.Font.Color := clMaroon;
   dbgTitulos.DefaultDrawDataCell(Rect, dbgTitulos.columns[datacol].field, State);
 end;

end;

procedure TFrmAcerto.edtChequeExit(Sender: TObject);
begin
 if edtCheque.Text = EmptyStr then
  edtCheque.Text := '0,00';
end;

procedure TFrmAcerto.edtChequeKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = #13 then
 begin
  calculaValorPago;
 end
 else
 begin
   FormataMoeda(Sender,Key);
 end;
end;

procedure TFrmAcerto.edtCreditoExit(Sender: TObject);
begin
 if edtCredito.Text = EmptyStr then
  edtCredito.Text := '0,00';
end;

procedure TFrmAcerto.edtCreditoKeyPress(Sender: TObject; var Key: Char);
begin
if Key = #13 then
 begin
  calculaValorPago;
 end
 else
 begin
   FormataMoeda(Sender,Key);
 end;
end;

procedure TFrmAcerto.edtDebitoClick(Sender: TObject);
begin
 edtDebito.SelectAll;
end;

procedure TFrmAcerto.edtDebitoExit(Sender: TObject);
begin
  if edtDebito.Text = EmptyStr then
    edtDebito.Text := '0,00';
end;

procedure TFrmAcerto.edtDebitoKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = #13 then
 begin
  calculaValorPago;
 end
 else
 begin
   FormataMoeda(Sender,Key);
 end;
end;

procedure TFrmAcerto.edtDinheiroClick(Sender: TObject);
begin
 edtDinheiro.SelectAll;
end;

procedure TFrmAcerto.edtDinheiroExit(Sender: TObject);
begin
 if edtDinheiro.Text = EmptyStr then
  edtDinheiro.Text := '0,00';
end;

procedure TFrmAcerto.edtDinheiroKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = #13 then
 begin
  calculaValorPago;
 end
 else
 begin
   FormataMoeda(Sender,Key);
 end;
end;

procedure TFrmAcerto.FormataMoeda(Componente: TObject; var Key: Char);
var
   str_valor  : String;
   dbl_valor  : double;
begin
    { verificando se estamos recebendo o TEdit realmente }
   IF (Componente is TLabeledEdit) or
      (Componente is TEdit) THEN
   BEGIN
      { se tecla pressionada e' um numero, backspace ou del deixa passar }
      IF ( Key in ['0'..'9', #8, #9] ) THEN
      BEGIN
         { guarda valor do TEdit com que vamos trabalhar }
         str_valor := TEdit( Componente ).Text ;
         { verificando se nao esta vazio }
         IF str_valor = EmptyStr THEN str_valor := '0,00' ;
         { se valor numerico ja insere na string temporaria }
         IF Key in ['0'..'9'] THEN str_valor := Concat( str_valor, Key ) ;
         { retira pontos e virgulas se tiver! }
         str_valor := Trim( StringReplace( str_valor, '.', '', [rfReplaceAll, rfIgnoreCase] ) ) ;
         str_valor := Trim( StringReplace( str_valor, ',', '', [rfReplaceAll, rfIgnoreCase] ) ) ;
         {inserindo 2 casas decimais}
         dbl_valor := StrToFloat( str_valor ) ;
         dbl_valor := ( dbl_valor / 100 ) ;

         {reseta posicao do tedit}
         TEdit( Componente ).SelStart := Length( TEdit( Componente ).Text );
         {retornando valor tratado ao TEdit}
         TEdit( Componente ).Text := FormatFloat( '#####0.00', dbl_valor ) ;
      END;
      {se nao e' key relevante entao reseta}
      IF NOT( Key in [#8, #9] ) THEN key := #0;
   END;

end;

procedure TFrmAcerto.FormCreate(Sender: TObject);
begin
 getCC;
 Visualizando := false;
end;

procedure TFrmAcerto.FormShow(Sender: TObject);
begin
 cdsAcerto.CreateDataSet;
 dtPesquisaI.Date := Now - 7;
 dtPesquisaF.Date := Now;
 mudarTela('tbPesquisa');
end;

procedure TFrmAcerto.getCC;
begin
 with dm.QryTemp,SQL do
 begin
   Clear;
   Close;
   Add('select * from fin_contacorrente cc');
   Add('order by cc.cod');
   Open;
 end;


 cbxCC.Clear;
 cbxCC.Items.Add('SELECIONE UMA CONTA');
 DM.QryTemp.First;
 while not DM.QryTemp.Eof do
 begin
   cbxcc.Items.Add(DM.QryTemp.FieldByName('NOME').AsString);
   dm.QryTemp.Next;
 end;
 cbxCC.ItemIndex := 0;
end;


procedure TFrmAcerto.limpaTela(ATela: string);
begin
 if ATela = 'tbTitulos' then
 begin
   cdsTitulos.Close;
   edtPesquisaTitulos.Text := EmptyStr;
 end
 else
 if ATela = 'tbAcerto' then
 begin
  if Visualizando = False then
  begin
   cdsAcerto.EmptyDataSet;
   cdsAcerto.Close;
   edtDinheiro.Text := '0,00';
   edtCheque.Text   := '0,00';
   edtDebito.Text   := '0,00';
   edtCredito.Text  := '0,00';
   edtVLPagoAcerto.Text  := '0,00';
   edtVLTotalAcerto.Text := '0,00';
   cbxCC.ItemIndex := 0;
  end
  else
  begin
   cdsAcerto.EmptyDataSet;
   cdsAcerto.Close;
   edtDinheiro.Text := '0,00';
   edtCheque.Text   := '0,00';
   edtDebito.Text   := '0,00';
   edtCredito.Text  := '0,00';
   edtVLPagoAcerto.Text  := '0,00';
   edtVLTotalAcerto.Text := '0,00';
   cbxCC.ItemIndex := 0;
   edtDebito.Enabled        := True;
   edtDinheiro.Enabled      := True;
   edtCheque.Enabled        := True;
   edtCredito.Enabled       := True;
   edtVLPagoAcerto.Enabled  := True;
   edtVLTotalAcerto.Enabled := True;
   Visualizando := False;
  end;
 end;
end;

procedure TFrmAcerto.mudarTela(ATela: string);
begin
 if ATela = 'tbPesquisa' then
 begin
   PageControl1.ActivePage := tbPesquisa;
   tbPesquisa.TabVisible   := True;
   tbAcerto.TabVisible     := False;
   tbTitulos.TabVisible   := False;
 end
 else
 if ATela = 'tbAcerto' then
 begin
   PageControl1.ActivePage := tbAcerto;
   tbPesquisa.TabVisible   := False;
   tbAcerto.TabVisible     := True;
   tbTitulos.TabVisible   := False;
 end
 else
 begin
   PageControl1.ActivePage := tbTitulos;
   tbPesquisa.TabVisible   := False;
   tbAcerto.TabVisible     := False;
   tbTitulos.TabVisible   := True;
 end;
end;

procedure TFrmAcerto.pesquisaAcertos;
var
 S: TStringList;
begin
 try
  S := TStringList.Create;

  if cdsPesquisa.Active then
    cdsPesquisa.EmptyDataSet;

  cdsPesquisa.Close;
  S.Add('   select a.cod, a.data, a.codcaixa,                                         ');
  S.Add('        case when a.tpoperacao = 1 then cast(''PAGAMENTO'' as varchar(15))   ');
  S.Add('             when a.tpoperacao = 2 then cast(''RECEBIMENTO'' as varchar(15)) ');
  S.Add('        end as TPOperacao,                                                   ');
  S.Add('        a.status, cc.nome, cc.cod as codCC,                                  ');
  S.Add('        pg.cod as codpgto, pg.dinheiro, pg.cheque, pg.credito, pg.debito,    ');
  S.Add('        sum(pg.dinheiro + pg.cheque + pg.credito + pg.debito) as total       ');
  S.Add('   from fin_acerto a                                                         ');
  S.Add('     join fin_pgto   pg on (a.cod = pg.codacerto)                            ');
  S.Add('     join fin_caixa  cx on (cx.cod = a.codcaixa)                             ');
  S.Add('     join fin_contacorrente cc on (cc.cod = cx.conta)                        ');
  {
  S.Add('     left join fin_docparcela par on (par.codacerto = a.cod)                 ');
  S.Add('     left join fin_documento  doc on (doc.cod = par.coddocumento)            ');
  S.Add('     left join clientes       cli on (cli.clienteid = doc.codpessoa)         ');
  }
  S.Add('   where 1=1                                                                 ');

  S.Add('   and a.data between '+ QuotedStr(GetDataFormatada(dtPesquisaI.Date))+' and '+
                                  QuotedStr(GetDataFormatada(dtPesquisaF.Date))        );

  if edtPesquisa.Text <> EmptyStr then
  begin
   if cbxTpPesquisa.ItemIndex = 0 then
      S.Add(' and a.cod = '+ edtPesquisa.Text                       );
{   else if cbxTpPesquisa.ItemIndex = 1 then
      S.Add(' and doc.codpessoa = '+ edtPesquisa.Text               )
   else if cbxTpPesquisa.ItemIndex = 2 then
      S.Add(' and doc.numdocumento = '+ QuotedStr(edtPesquisa.Text) )
   else if cbxTpPesquisa.ItemIndex = 3 then
      S.Add(' and cli.nome like '+ QuotedStr(edtPesquisa.Text+'%')      ); }
  end;

  S.Add('  group by a.cod, a.data, a.codcaixa, a.tpoperacao, a.status, cc.nome, cc.cod,');
  S.Add('           pg.cod, pg.dinheiro, pg.cheque, pg.credito, pg.debito             ');

  cdsPesquisa.CommandText := S.Text;
  cdsPesquisa.Open;
 finally
  FreeAndNil(S);
 end;

end;

procedure TFrmAcerto.pesquisaTitulos;
var
 S : TStringList;
begin
 try
  if cdsTitulos.Active then
    cdsTitulos.EmptyDataSet;

  cdsTitulos.Close;
  S := TStringList.Create;
  S.Add(' select d.cod,d.codpessoa,CAST (d.codpessoa|| '' - '' || c.nome as varchar(50)) as Pessoa, (d.numdocumento||''/''||p.parcela)  as DocParcela, d.tpmovimento, ');
  S.Add('        p.valor, p.vencimento, p.status, c.nome, D.numdocumento, p.parcela, p.codparcela, d.dataemissao, (cast ('' '' as varchar(1))) as mk,');
  S.Add('        (case when d.tpmovimento = 1 then cast (d.tpmovimento|| ''- PAGAR'' as varchar(20))  ');
  S.Add('             when d.tpmovimento = 2 then cast (d.tpmovimento|| ''- RECEBER'' as varchar(20)) ');
  S.Add('        end ) AS TIPOMOV');
  S.Add(' from fin_documento d');
  S.Add('  join fin_docparcela p on (d.cod = p.coddocumento)');
  S.Add('  join clientes       c on (d.codpessoa = c.clienteid)');
  S.Add('  where p.status = ''ABE''  ');

  if edtPesquisaTitulos.Text <> EmptyStr then
  begin
    if cbxTitulos.ItemIndex = 2 then
     S.Add('and d.numdocumento = '+ QuotedStr(edtPesquisaTitulos.Text))
    else if cbxTitulos.ItemIndex = 0 then
     S.Add('and c.clienteid = ' + edtPesquisaTitulos.Text)
    else if cbxTitulos.ItemIndex = 1 then
     S.Add('and c.nome like ' + QuotedStr(edtPesquisaTitulos.Text+'%') );
  end;

  if cbxDataTitulos.Text = 'VENCIMENTO' then
  begin
    S.Add('and p.vencimento between '+QuotedStr(GetDataFormatada(dtTitulosI.Date))+' and '+QuotedStr(GetDataFormatada(dtTitulosF.Date))  );
  end
  else if cbxDataTitulos.Text = 'EMISSAO' then
  begin
    S.Add('and d.dataemissao between '+QuotedStr(GetDataFormatada(dtTitulosI.Date))+' and '+QuotedStr(GetDataFormatada(dtTitulosF.Date))  );
  end;


  if TpMov = Pagar then
  begin
    S.Add('and d.tpmovimento = 1');
  end
  else if TpMov = Receber then
  begin
    S.Add('and d.tpmovimento = 2');
  end;

  cdsTitulos.CommandText := s.Text;
  cdsTitulos.Open;
 finally
  FreeAndNil(S);
 end;

end;


procedure TFrmAcerto.realizaAcerto(ATpAcerto: TTipoMov);
var
 codAcerto : Integer;
begin
 if cbxCC.ItemIndex = 0 then
 begin
   ShowMessage('Selecione uma conta para realizar a opera��o');
   Abort;
 end;

 if StrToFloat(edtVLPagoAcerto.Text) <> StrToFloat(edtVLTotalAcerto.Text) then
 begin
  ShowMessage('O Valor Pago n�o pode ser diferente do Valor Total');
  Abort;
 end;

 if VerificaCaixaAberto(cbxCC.ItemIndex) = False then
 BEGIN
   ShowMessage('N�o � possivel lan�ar acerto em caixa fechado');
   Abort;
 END;

 with DM do
 begin
  try
   cdsFin_Acerto.Open;
   cdsFin_Acerto.Append;
   DM.cdsFin_AcertoDATA.Value       := Now;
   DM.cdsFin_AcertoCODCAIXA.Value   := GetCodCaixa(cbxCC.ItemIndex);
   if ATpAcerto = Receber then
   begin
    DM.cdsFin_AcertoTPOPERACAO.Value := 2;
   end
   else if ATpAcerto = Pagar then
   begin
    DM.cdsFin_AcertoTPOPERACAO.Value := 1;
   end;

   DM.cdsFin_AcertoSTATUS.Value     := 'CON';
   DM.cdsFin_Acerto.Post;
   DM.cdsFIN_PGTO.Open;
   DM.cdsFIN_PGTO.Append;
   DM.cdsFIN_PGTO.FieldByName('CODACERTO').Value := DM.cdsFin_AcertoCOD.Value;
   codAcerto := DM.cdsFin_AcertoCOD.Value;
   if ATpAcerto = Receber then
   begin
    DM.cdsFIN_PGTO.FieldByName('TPMOV').Value := 'C';
   end
   else if ATpAcerto = Pagar then
   begin
    DM.cdsFIN_PGTO.FieldByName('TPMOV').Value := 'D';
   end;

   DM.cdsFIN_PGTO.FieldByName('DINHEIRO').Value  := StrToFloat(edtDinheiro.Text);
   DM.cdsFIN_PGTO.FieldByName('CHEQUE').Value    := StrToFloat(edtCheque.Text);
   DM.cdsFIN_PGTO.FieldByName('CREDITO').Value   := StrToFloat(edtCredito.Text);
   DM.cdsFIN_PGTO.FieldByName('DEBITO').Value    := StrToFloat(edtDebito.Text);
   DM.cdsFIN_PGTO.Post;

   GravarLog(DM.cdsFin_AcertoCOD.Value,'Acerto Financeiro');
   DM.cdsFin_Acerto.ApplyUpdates(0);
   AtualizaTitulos(codAcerto);
  except
   DM.cdsFin_Acerto.CancelUpdates;
  end;
 end;



end;

procedure TFrmAcerto.tbTitulosShow(Sender: TObject);
begin
 dtTitulosI.Date := Now - 7;
 dtTitulosF.Date := Now;
end;

procedure TFrmAcerto.visualizaTitulos(ACodAcerto:Integer);
var
 total: Double;
begin
   with DM.QryTemp,SQL do
   begin
    Clear;
    Close;
    Add('  select d.cod, CAST (d.codpessoa|| '' - '' || c.nome as varchar(50)) as Pessoa, (d.numdocumento||''/''||p.parcela)  as DocParcela, d.tpmovimento,');
    Add('       p.valor, p.vencimento, p.status, c.nome, D.numdocumento, p.parcela, p.codparcela, d.dataemissao,');
    Add('       (case when d.tpmovimento = 1 then cast (d.tpmovimento|| ''- PAGAR'' as varchar(20))');
    Add('             when d.tpmovimento = 2 then cast (d.tpmovimento|| ''- RECEBER'' as varchar(20))');
    Add('       end ) AS TIPOMOV');
    Add('  from fin_documento d');
    Add('  join fin_docparcela p on (d.cod = p.coddocumento)');
    Add('  join clientes       c on (d.codpessoa = c.clienteid)');
    Add('  where p.codacerto = '+ IntToStr(ACodAcerto));
    Open;
   end;

   DM.QryTemp.First;
   while not dm.QryTemp.Eof do
   begin
    cdsAcerto.Open;
    cdsAcerto.Append;
    cdsAcertoPESSOA.Value     := dm.QryTemp.FieldByName('PESSOA').Value;
    cdsAcertoDOCUMENTO.Value  := dm.QryTemp.FieldByName('DOCPARCELA').Value;
    cdsAcertoVENCIMENTO.Value := dm.QryTemp.FieldByName('VENCIMENTO').Value;
    cdsAcertoVALOR.Value      := dm.QryTemp.FieldByName('VALOR').Value;
    cdsAcertoTIPO.Value       := dm.QryTemp.FieldByName('TIPOMOV').Value;
    cdsAcertoEMISSAO.Value    := dm.QryTemp.FieldByName('DATAEMISSAO').Value;
    cdsAcertoCOD.Value        := dm.QryTemp.FieldByName('COD').Value;
    cdsAcerto.Post;
    DM.QryTemp.Next;
   end;

   with dm.QryTemp,SQL do
   begin
     Clear;
     Close;
     Add('select pg.*, cc.cod as codcc from fin_pgto pg');
     Add('join fin_acerto a on (a.cod = pg.codacerto)');
     Add('join fin_caixa cx on (a.codcaixa = cx.cod)');
     Add('join fin_contacorrente cc on (cc.cod = cx.conta)');
     Add('where pg.codacerto = '+ IntToStr(ACodAcerto));
     Open;
   end;
   edtDinheiro.Text := FormatFloat('##0.00', DM.QryTemp.FieldByName('DINHEIRO').Value);
   edtCheque.Text   := FormatFloat('##0.00', DM.QryTemp.FieldByName('CHEQUE').Value);
   edtCredito.Text  := FormatFloat('##0.00', DM.QryTemp.FieldByName('CREDITO').Value);
   edtDebito.Text   := FormatFloat('##0.00', DM.QryTemp.FieldByName('DEBITO').Value);
   cbxCC.ItemIndex := dm.QryTemp.FieldByName('CODCC').Value;

   edtDebito.Enabled        := False;
   edtDinheiro.Enabled      := False;
   edtCheque.Enabled        := False;
   edtCredito.Enabled       := False;
   total := DM.QryTemp.FieldByName('DINHEIRO').Value + DM.QryTemp.FieldByName('CHEQUE').Value +
            DM.QryTemp.FieldByName('CREDITO').Value  + DM.QryTemp.FieldByName('DEBITO').Value;
   edtVLTotalAcerto.Text := FormatFloat('##0.00',total);
   edtVLPagoAcerto.Text  := FormatFloat('##0.00',total);
   edtVLTotalAcerto.Enabled := False;
   edtVLPagoAcerto.Enabled  := False;
end;

end.
