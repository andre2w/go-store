unit uFrmUsuario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uDM, uFrmModelo, Vcl.Mask, Vcl.DBCtrls,
  Data.DB, Vcl.ImgList, Vcl.ActnList, Vcl.Menus, Vcl.ComCtrls, Vcl.ToolWin,
  Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  System.Actions, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh;

type
  TFrmUsuario = class(TFrmModelo)
    Label5: TLabel;
    DBEdit2: TDBEdit;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    Label4: TLabel;
    cbVendedor: TDBCheckBox;
    DBLookupComboBox1: TDBLookupComboBox;
    cbMaster: TDBCheckBox;
    cbStatus: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure ac_PesquisarExecute(Sender: TObject);
    procedure ac_SalvarExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmUsuario: TFrmUsuario;

implementation

{$R *.dfm}

uses uFrmAlteraSenha, uUsuario, Datasnap.DBClient, uRotinas;

procedure TFrmUsuario.ac_PesquisarExecute(Sender: TObject);
begin
   if RadioGroup1.ItemIndex = 0 then
 begin
   DM.sdsUsuario.CommandText := 'select * from USUARIOS where STATUS = ''A'' ';
 end
 else
 begin
   DM.sdsUsuario.CommandText := 'select * from USUARIOS where STATUS = ''I'' ';
 end;
  inherited;

end;

procedure TFrmUsuario.ac_SalvarExecute(Sender: TObject);
begin
  GravarLog(DM.cdsUsuariosUSUARIOID.Value,'CAD USUARIOS');
  inherited;

end;

procedure TFrmUsuario.FormCreate(Sender: TObject);
begin
  inherited;
  if not TUsuario.GetInstance.IsMaster then
  begin
    TClientDataSet(Ds.DataSet).CommandText := 'Select * from USUARIOS where USUARIOID =' + IntToStr(TUsuario.GetInstance.ID);
  end;
end;

end.

