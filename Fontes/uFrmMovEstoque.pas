unit uFrmMovEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModelo, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Mask, Data.DB, Vcl.ImgList, Vcl.ActnList, Vcl.Menus,
  Vcl.ComCtrls, Vcl.ToolWin, Vcl.Grids, Vcl.DBGrids, Vcl.Buttons, Vcl.DBCtrls,
  Datasnap.DBClient, System.Actions;

type
  TFrmMovEstoque = class(TFrmModelo)
    DsDetail: TDataSource;
    DBGrid2: TDBGrid;
    cbxTpMovimentacao: TComboBox;
    Label4: TLabel;
    Label5: TLabel;
    GroupBox1: TGroupBox;
    btnPesquisaProduto: TBitBtn;
    edtPesquisaProduto: TLabeledEdit;
    cbxTpPesquisa: TComboBox;
    Label6: TLabel;
    Label7: TLabel;
    DataSource1: TDataSource;
    edtDescricaoProduto: TLabeledEdit;
    Label8: TLabel;
    ac_PesquisaProduto: TAction;
    btnAdicionar: TBitBtn;
    edtCodProduto: TEdit;
    edtQTDE: TEdit;
    cdsMovProdutos: TClientDataSet;
    cdsMovProdutosREFERENCIA: TStringField;
    cdsMovProdutosDESCRICAO: TStringField;
    cdsMovProdutosCODPROD: TIntegerField;
    cdsMovProdutosQTDE: TIntegerField;
    cdsMovProdutosTOTALQTDE: TAggregateField;
    edtReferencia: TEdit;
    Label9: TLabel;
    dtInclusao: TDateTimePicker;
    procedure ac_PesquisaProdutoExecute(Sender: TObject);
    procedure btnAdicionarClick(Sender: TObject);
    procedure edtQTDEKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure ac_IncluirExecute(Sender: TObject);
    procedure ac_SalvarExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmMovEstoque: TFrmMovEstoque;

implementation

{$R *.dfm}

uses uDM, uFrmPesqProduto;

procedure TFrmMovEstoque.ac_IncluirExecute(Sender: TObject);
begin
  inherited;
  cdsMovProdutos.Open;
end;

procedure TFrmMovEstoque.ac_PesquisaProdutoExecute(Sender: TObject);
begin
  inherited;
 FrmPesqProduto := TFrmPesqProduto.Create(nil);
 FrmPesqProduto.edtPesquisaProduto.Text := edtPesquisaProduto.Text;
 FrmPesqProduto.cbxTpPesquisa.ItemIndex := cbxTpPesquisa.ItemIndex;
 FrmPesqProduto.btnPesquisaProduto.Click;
 if FrmPesqProduto.ShowModal = mrOk then
 begin
  edtCodProduto.Text := DM.cdsConsultaProduto.FieldByName('PRODUTOID').AsString;
  edtDescricaoProduto.Text := Dm.cdsConsultaProduto.FieldByName('DESCRICAO').AsString;
  edtReferencia.Text := dm.cdsConsultaProduto.FieldByName('REFERENCIA').AsString;
  edtQTDE.Text := '1';
  edtQTDE.SetFocus;
 end;
 FreeAndNil(FrmPesqProduto);
end;

procedure TFrmMovEstoque.ac_SalvarExecute(Sender: TObject);
VAR
 tpmov:integer;
begin
 if cbxTpMovimentacao.Text = '<SELECIONE>' then
 begin
   MessageDlg('Favor selecionar o Tipo de Movimentação!',mtWarning,[mbOK],0);
   exit;
 end
 else if cbxTpMovimentacao.Text = '1 - ENTRADA DE ESTOQUE' then
 begin
   tpmov := 1;
 end
 else
 begin
   tpmov := 2;
 end;

  if not tpmov in [1,2] then
  begin
   inherited;
  end
  else
  begin
    dm.cdsMov_Estoque.Open;
    dm.cdsMov_Estoque.Append;
    dm.cdsMov_EstoqueQTDEPECAS.Value := cdsMovProdutosTOTALQTDE.Value;
    dm.cdsMov_EstoqueDATA.Value := dtInclusao.Date;
    dm.cdsMov_EstoqueTIPO_MOVIMENTO.Value := tpmov;
    cdsMovProdutos.First;
    while not cdsMovProdutos.Eof do
    begin
      dm.cdsMovEstoqueItens.Open;
      dm.cdsMovEstoqueItens.Append;
      dm.cdsMovEstoqueItens.FieldByName('CODPRODUTO').Value := cdsMovProdutosCODPROD.Value;
      dm.cdsMovEstoqueItens.FieldByName('QTDE').Value := cdsMovProdutosQTDE.Value;
      dm.cdsMovEstoqueItens.Post;
      cdsMovProdutos.Next;
    end;
    dm.cdsMov_Estoque.ApplyUpdates(0);
    dm.cdsMov_Estoque.Close;
  end;
end;

procedure TFrmMovEstoque.btnAdicionarClick(Sender: TObject);
begin
  inherited;
 cdsMovProdutos.Append;
 cdsMovProdutosREFERENCIA.Value := edtReferencia.Text;
 cdsMovProdutosCODPROD.Value := StrToInt(edtCodProduto.Text);
 cdsMovProdutosDESCRICAO.Value := edtDescricaoProduto.Text;
 cdsMovProdutosQTDE.Value := StrToInt(edtQTDE.Text);
 cdsMovProdutos.Post;
end;

procedure TFrmMovEstoque.edtQTDEKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
 if key = #13 then
 begin
   btnAdicionar.SetFocus;
 end;
end;

procedure TFrmMovEstoque.FormCreate(Sender: TObject);
begin
  inherited;
  cdsMovProdutos.CreateDataSet;
end;

end.
