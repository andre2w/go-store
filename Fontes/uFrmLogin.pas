unit uFrmLogin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, uDM, uUsuario, uFrmPrincipal, registry;

type
  TFrmLogin = class(TForm)
    Image1: TImage;
    edtUsuario: TLabeledEdit;
    edtSenha: TLabeledEdit;
    btnLogin: TBitBtn;
    btnCancelar: TBitBtn;
    CheckBox1: TCheckBox;
    procedure btnLoginClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edtUsuarioKeyPress(Sender: TObject; var Key: Char);
    procedure edtSenhaKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    procedure GravaUsuario;
  public
    { Public declarations }
  end;

var
  FrmLogin: TFrmLogin;

implementation

{$R *.dfm}

procedure TFrmLogin.btnCancelarClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFrmLogin.btnLoginClick(Sender: TObject);
begin
 if TUsuario.GetInstance.Login(edtUsuario.Text,edtSenha.Text,FrmPrincipal.Acoes) then
 begin
  if CheckBox1.Checked then
   GravaUsuario;
  ModalResult := mrOK;
 end
 else
 begin
   MessageDlg('Usu�rio ou Senha Incorretos',mtWarning,[mbOK],0);
   edtSenha.SelectAll;
   edtSenha.SetFocus;
 end;

end;

procedure TFrmLogin.edtSenhaKeyPress(Sender: TObject; var Key: Char);
begin
 if key = #13 then
  btnLogin.SetFocus;
end;

procedure TFrmLogin.edtUsuarioKeyPress(Sender: TObject; var Key: Char);
begin
 if key = #13 then
    edtSenha.SetFocus;
end;

procedure TFrmLogin.FormCreate(Sender: TObject);
var
 Reg : TRegistry;
begin
 Reg := TRegistry.Create(HKEY_CURRENT_USER);
 if reg.OpenKey('GO\COMERCIAL',True) then
 begin
  edtUsuario.Text := reg.ReadString('UserName');
  CheckBox1.Checked := True;
 end;
end;

procedure TFrmLogin.GravaUsuario;
var
 Reg: TRegistry;
begin
 Reg := TRegistry.Create;
 if Reg.OpenKey('GO\COMERCIAL',true) then
 begin
   reg.WriteString('UserName',edtUsuario.Text);
 end;

end;

end.
