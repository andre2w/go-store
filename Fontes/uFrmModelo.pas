unit uFrmModelo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.Grids,
  Vcl.DBGrids, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.Menus, Vcl.ActnList, Vcl.ImgList,
  Vcl.ToolWin, Data.DB, System.Actions, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh;

type
  TFrmModelo = class(TForm)
    ActionList1: TActionList;
    ImageList1: TImageList;
    ac_Incluir: TAction;
    ac_Excluir: TAction;
    ac_Alterar: TAction;
    ac_Imprimir: TAction;
    ac_Salvar: TAction;
    ac_Cancelar: TAction;
    ac_Fechar: TAction;
    PageControl1: TPageControl;
    tbCadastro: TTabSheet;
    tbConsulta: TTabSheet;
    Panel1: TPanel;
    cbxCampo: TComboBox;
    edtPesquisa: TEdit;
    btnPesquisar: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Ds: TDataSource;
    ac_Vizualizar: TAction;
    Label3: TLabel;
    ac_Pesquisar: TAction;
    edtRegistros: TEdit;
    lblNumeroRegistro: TLabel;
    lblMaisRegistros: TLabel;
    RadioGroup1: TRadioGroup;
    pnBottomCadastro: TPanel;
    pnBottomConsulta: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    DBGrid1: TDBGridEh;

    procedure ac_IncluirExecute(Sender: TObject);
    procedure ac_ExcluirExecute(Sender: TObject);
    procedure ac_AlterarExecute(Sender: TObject);
    procedure ac_ImprimirExecute(Sender: TObject);
    procedure ac_VizualizarExecute(Sender: TObject);
    procedure ac_CancelarExecute(Sender: TObject);
    procedure ac_FecharExecute(Sender: TObject);
    procedure ac_SalvarUpdate(Sender: TObject);
    procedure Label3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ac_PesquisarExecute(Sender: TObject);
    procedure lblMaisRegistrosClick(Sender: TObject);
    procedure mudarTab(ANomeTab:TTabSheet);
  private
    { Private declarations }
    ListaField: Array of integer;
    Pacotes: Integer;
    procedure numeroRegistros;
  public
    { Public declarations }
  end;

var
  FrmModelo: TFrmModelo;

implementation

uses
  Datasnap.DBClient, uDM, uUsuario;

{$R *.dfm}

procedure TFrmModelo.ac_PesquisarExecute(Sender: TObject);
begin
 if edtRegistros.Text = '' then
  edtRegistros.Text := '1';

 ds.DataSet.Close;
 TClientDataSet(DS.DataSet).PacketRecords := StrToInt(edtRegistros.Text);
 if (cbxCampo.Text <> EmptyStr) and (edtPesquisa.Text <> EmptyStr) then
  begin
   TClientDataSet(DS.DataSet).PacketRecords := StrToInt(edtRegistros.Text);
   Ds.DataSet.Filter := 'Upper(' + cbxCampo.Text +') like '+ QuotedStr(edtPesquisa.Text+'%');
   Ds.DataSet.Filtered := True;
   if not ds.DataSet.Active then
    Ds.DataSet.Open;
   Label3.Visible := True;
  end
 else
 begin
  if not ds.DataSet.Active then
    Ds.DataSet.Open;
 end;
end;

procedure TFrmModelo.ac_AlterarExecute(Sender: TObject);
begin
 if Ds.DataSet.Active and not ds.DataSet.IsEmpty  then
 begin
  PageControl1.ActivePage := tbCadastro;
  Ds.DataSet.Edit;
 end;
end;

procedure TFrmModelo.ac_CancelarExecute(Sender: TObject);
begin
   TClientDataSet(Ds.DataSet).CancelUpdates;
 PageControl1.ActivePage := tbConsulta;
end;

procedure TFrmModelo.ac_ExcluirExecute(Sender: TObject);
begin
 if Application.MessageBox('Deseja excluir este registro?','Excluir',MB_YESNO+MB_ICONQUESTION)= id_yes then
 begin
  if not ds.DataSet.IsEmpty then
  begin
   Ds.DataSet.Delete;
    if Ds.DataSet is TClientDataSet then
     TClientDataSet(Ds.DataSet).ApplyUpdates(0);
  end;
 end
 else
 begin
   Exit;
 end;
end;

procedure TFrmModelo.ac_FecharExecute(Sender: TObject);
begin
 if PageControl1.ActivePage = tbCadastro then
 begin
   if Ds.DataSet.State in [dsInsert,dsEdit] then
    TClientDataSet(Ds.DataSet).CancelUpdates;

   mudarTab(tbConsulta);
 end
 else
 begin
  Ds.DataSet.Close;
  Close;
 end;

end;

procedure TFrmModelo.ac_ImprimirExecute(Sender: TObject);
begin
 ShowMessage('Implementação futura');
end;

procedure TFrmModelo.ac_IncluirExecute(Sender: TObject);
begin
 if PageControl1.ActivePage = tbConsulta then
 begin
  if not ds.DataSet.Active then
   Ds.DataSet.Open;

  Ds.DataSet.Insert;
  mudarTab(tbCadastro);
 end
 else
 begin
  TClientDataSet(Ds.DataSet).ApplyUpdates(0);
  mudarTab(tbConsulta);
 end;
end;

procedure TFrmModelo.ac_SalvarUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled := Ds.DataSet.State in [dsInsert,dsEdit];
 Label3.Visible := Ds.DataSet.Filtered;
 lblMaisRegistros.Visible := Pacotes > 0;
end;

procedure TFrmModelo.ac_VizualizarExecute(Sender: TObject);
begin
 mudarTab(tbCadastro);
end;

procedure TFrmModelo.FormCreate(Sender: TObject);
var
 i,j:integer;
begin
for I := 0 to Pred(Ds.DataSet.FieldCount) do
 begin
   if ds.DataSet.Fields[i].DataType in [ftString,ftWideString,ftFixedChar] then
   begin
    j:= cbxCampo.Items.Add(ds.DataSet.Fields[i].DisplayLabel);
//    SetLength(ListaField,High(ListaField)+2);
//    ListaField[I] := I;
// Esses dois campos foram comentados para carregar o primeiro item no cbxCampo no FormCreate
   end;
 end;
 edtRegistros.Text := '100';
 TClientDataSet(Ds.DataSet).PacketRecords := StrToInt(edtRegistros.Text);
// Ds.DataSet.Open;
 Pacotes := 1;
 mudarTab(tbConsulta);
 numeroRegistros;
end;

procedure TFrmModelo.FormShow(Sender: TObject);
begin
 Label3.Visible := False;
 PageControl1.ActivePage := tbConsulta;
 cbxCampo.ItemIndex := 0;
end;

procedure TFrmModelo.Label3Click(Sender: TObject);
begin
 Ds.DataSet.Filtered := false;
 edtPesquisa.Text    := EmptyStr;
 Label3.Visible      := False;
end;

procedure TFrmModelo.lblMaisRegistrosClick(Sender: TObject);
begin
  TClientDataSet(ds.DataSet).GetNextPacket;
end;

procedure TFrmModelo.mudarTab(ANomeTab: TTabSheet);
var
  I: Integer;
begin
 PageControl1.ActivePage := ANomeTab;
 for I := 0 to Pred(PageControl1.PageCount) do
 begin
  if ANomeTab = PageControl1.Pages[i] then
   PageControl1.Pages[i].TabVisible := True
  else
   PageControl1.Pages[i].TabVisible := False;
 end;

end;

procedure TFrmModelo.numeroRegistros;
begin
 DBGrid1.FooterRowCount := 1;
 DBGrid1.Columns[0].Footer.FieldName := TClientDataSet(ds.DataSet).Fields[0].FieldName;
 DBGrid1.Columns[0].Footer.ValueType := fvtSum;

end;

end.
