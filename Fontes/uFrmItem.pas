unit uFrmItem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls;

type
  TFrmItem = class(TForm)
    edtQuantidade: TLabeledEdit;
    edtPreco: TLabeledEdit;
    BitBtn1: TBitBtn;
    btnCancelar: TBitBtn;
    Panel1: TPanel;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmItem: TFrmItem;

implementation

{$R *.dfm}

procedure TFrmItem.BitBtn1Click(Sender: TObject);
begin
 ModalResult := mrOk;
end;

procedure TFrmItem.btnCancelarClick(Sender: TObject);
begin
 ModalResult := mrCancel;
end;

procedure TFrmItem.FormKeyPress(Sender: TObject; var Key: Char);
begin
 if key = #27 then
  ModalResult := mrCancel;
end;

procedure TFrmItem.FormShow(Sender: TObject);
begin
 edtPreco.Text := FormatFloat('##0.00',StrToFloat(edtPreco.Text));
end;

end.
