unit uDMReport;

interface

uses
  System.SysUtils, System.Classes, Data.FMTBcd, Datasnap.DBClient,
  Datasnap.Provider, Data.DB, Data.SqlExpr, frxClass, frxDBSet, Data.DBXFirebird;

type
  TdmReport = class(TDataModule)
  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  dmReport: TdmReport;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses uDM;

{$R *.dfm}

{ TdmReport }

end.
