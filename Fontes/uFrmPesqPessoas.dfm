object FrmPesqPessoa: TFrmPesqPessoa
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = 'Pesquisa de Pessoas'
  ClientHeight = 467
  ClientWidth = 799
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 799
    Height = 57
    Align = alTop
    TabOrder = 0
    object Label6: TLabel
      Left = 9
      Top = 14
      Width = 80
      Height = 13
      Caption = 'Tipo de Pesquisa'
    end
    object btnPesquisar: TBitBtn
      Left = 697
      Top = 28
      Width = 99
      Height = 25
      Caption = 'Pesquisar'
      Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C0060000430B0000430B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF646873183D
        60FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        646873467BCB4D5A91183D60FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FF5192C64CA5EF467BCB4D5A91183D60FF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FF3C98E498D8F84BA9F5467BCB4D5A9118
        3D60FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF3C98E498D8
        F84AA5F2467BCB4D5A91183D60FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF3C98E498D8F84BA9F5467BCB4D5A916D6676FF00FFC1AFAEBDA1
        9BC0A99EA992969FABB7FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FF3C98E498D8F851A7EF768BAA9D8481
        CFB0A5F0DEC6FAF1CAFEFBCDEEE0BFDDC4ADA49291FF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF3C98E46D
        6676AE958FD1BCADF8F0D2FFEDBEFFEBBBFFF1D1FFFAE2FFFDE1E9DBC6AA9A9A
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FF9FB8C4CCA89DFBF5D7FFE6B7FFE0AFFFEECFFFF2DEFFF4E5FF
        FDFDFFFFF9D5C1B2BEB9BCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFAE9A9BE8D5C0FFF4CBFFDAA3FFE5BCFFF0
        D9FFF0D5FFF5E8FFFEFEFFFEFBF2EBCED8C0B2FF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB99D8FFBF7DCFFEBBC
        FFD79FFFDFB2FFEFD8FFEDD6FFF0D9FFF4E2FFF3DFFCF0C3B99D8FFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB9
        9D8FFDFAE1FFEABCFFD79FFFD8A3FFDFB5FFEFDAFFEED7FFF1DAFFE4B7FEEFBD
        B99D8FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFA28E90ECDAC3FFFADBFFF2E1FFEFD9FFDBA7FFDDACFFE8C4FF
        E0AFFFE0A8FDF0CBB5ABA4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFDAB5A7FBF9EAFFFFFFFFF8F0FFDE
        B2FFD69CFFD89EFFDDA5FEF0C5E4D3BC969EA7FF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFD0B8B5E3CDBE
        FCF9EEFFFAE3FFE9BAFFE2B1FFE9BBFFF3CFE7DDC3C0A5A0FF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFD6BDB6DBBFAFEFE1C7FBF7D8FFF9DDF2E9CDCFB7A6C4A89FFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB79D98B99D8FB99D8FB68E89C3
        B7BAFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      TabOrder = 0
      OnClick = btnPesquisarClick
    end
    object edtPesquisa: TLabeledEdit
      Left = 160
      Top = 30
      Width = 531
      Height = 21
      CharCase = ecUpperCase
      EditLabel.Width = 69
      EditLabel.Height = 13
      EditLabel.Caption = 'Pesquisar Por:'
      TabOrder = 1
      OnKeyPress = edtPesquisaKeyPress
    end
    object cbxTpPesquisa: TComboBox
      Left = 9
      Top = 30
      Width = 145
      Height = 21
      ItemIndex = 1
      TabOrder = 2
      Text = 'NOME'
      Items.Strings = (
        'CODIGO'
        'NOME'
        'CIDADE')
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 57
    Width = 799
    Height = 369
    Align = alClient
    DataSource = dsPesqPessoa
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    OnKeyPress = DBGrid1KeyPress
  end
  object Panel1: TPanel
    Left = 0
    Top = 426
    Width = 799
    Height = 41
    Align = alBottom
    TabOrder = 2
    object btnConfirmar: TBitBtn
      Left = 3
      Top = 6
      Width = 94
      Height = 25
      Caption = 'Confirmar'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF006600006600FF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0066001EB2311FB13300
        6600FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF00660031C24F22B7381AB02D21B437006600FF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FF00660047D36D3BCB5E25A83B0066001B
        A92E1DB132006600FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF006600
        4FD67953DE7F31B54D006600FF00FF006600179D271EAE31006600FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FF00660041C563006600FF00FFFF00FFFF
        00FFFF00FF00660019AA2B006600FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF006600FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF006600149D210066
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FF006600006600FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FF006600006600FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      TabOrder = 0
      OnClick = btnConfirmarClick
    end
    object btnCancelar: TBitBtn
      Left = 702
      Top = 6
      Width = 94
      Height = 25
      Caption = 'Cancelar'
      Glyph.Data = {
        76030000424D7603000000000000360000002800000011000000100000000100
        18000000000040030000C40E0000C40E00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FF00FF00FFFF00FFFF00FF00009A00009AFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF00009A00009AFF00FFFF00FFFF00FFFF00FF00FF00FFFF00FF00009A17
        4AFD103BF400009AFF00FFFF00FFFF00FFFF00FF00009A002CF80030FC00009A
        FF00FFFF00FFFF00FF00FF00FFFF00FF00009A1A47F81A4CFF123BF100009AFF
        00FFFF00FF00009A012DF60132FF002AF300009AFF00FFFF00FFFF00FF00FF00
        FFFF00FFFF00FF00009A1C47F61B4DFF143EF400009A00009A002DF80134FF03
        2BF200009AFF00FFFF00FFFF00FFFF00FF00FF00FFFF00FFFF00FFFF00FF0000
        9A1D48F61D50FF103DFB0431FE0132FF002CF600009AFF00FFFF00FFFF00FFFF
        00FFFF00FF00FF00FFFF00FFFF00FFFF00FFFF00FF00009A1A48F91342FF0C3C
        FF0733F600009AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FF00FFFF00FF
        FF00FFFF00FFFF00FF00009A214EFC1D4BFF1847FF1743F600009AFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF00FF00FFFF00FFFF00FFFF00FF00009A2E5BF9
        2C5FFF224DF8204BF82355FF1B46F600009AFF00FFFF00FFFF00FFFF00FFFF00
        FF00FF00FFFF00FFFF00FF00009A3664FA386BFF2D59F400009A00009A224CF4
        2558FF1D49F600009AFF00FFFF00FFFF00FFFF00FF00FF00FFFF00FF00009A40
        71FA4274FF325DF100009AFF00FFFF00FF00009A224DF1275AFF204CF800009A
        FF00FFFF00FFFF00FF00FF00FFFF00FF00009A497AFC3B66F300009AFF00FFFF
        00FFFF00FFFF00FF00009A2550F42655FA00009AFF00FFFF00FFFF00FF00FF00
        FFFF00FFFF00FF00009A00009AFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00
        009A00009AFF00FFFF00FFFF00FFFF00FF00FF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00}
      TabOrder = 1
      OnClick = btnCancelarClick
    end
  end
  object dsPesqPessoa: TDataSource
    DataSet = DM.cdsPesquisaCliente
    Left = 752
    Top = 376
  end
end
