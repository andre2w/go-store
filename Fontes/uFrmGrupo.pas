unit uFrmGrupo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModelo, Data.DB, Vcl.ImgList,
  Vcl.ActnList, Vcl.Menus, Vcl.ComCtrls, Vcl.ToolWin, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons, Vcl.Mask, Vcl.DBCtrls, System.Actions;

type
  TFrmGrupo = class(TFrmModelo)
    Label4: TLabel;
    DBEdit1: TDBEdit;
    procedure ac_SalvarExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmGrupo: TFrmGrupo;

implementation

{$R *.dfm}

uses uDM, uRotinas;

procedure TFrmGrupo.ac_SalvarExecute(Sender: TObject);
begin
  GravarLog(DM.cdsGrupoCOD.Value,'CAD GRUPO');
  inherited;

end;

end.
