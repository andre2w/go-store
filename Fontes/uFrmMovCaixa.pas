unit uFrmMovCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.ExtCtrls, Data.DB,
  Vcl.StdCtrls, Vcl.DBCtrls, Vcl.Buttons, Vcl.Grids, Vcl.DBGrids, Data.FMTBcd,
  Datasnap.DBClient, Datasnap.Win.TConnect, Datasnap.Provider, Data.SqlExpr,
  System.Actions, Vcl.ActnList;

type
  TFrmMovCaixa = class(TForm)
    pnTop: TPanel;
    pnBottom: TPanel;
    PageControl1: TPageControl;
    tbCaixa: TTabSheet;
    tbMov: TTabSheet;
    lbCC: TLabel;
    btnPesquisar: TBitBtn;
    btnAbrirCX: TBitBtn;
    btnFecharCX: TBitBtn;
    BitBtn2: TBitBtn;
    dbgCaixa: TDBGrid;
    cbxCC: TComboBox;
    dtAberturaF: TDateTimePicker;
    dtAberturaI: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    sdsMovCaixa: TSQLDataSet;
    cdsMovCaixa: TClientDataSet;
    dspMovCaixa: TDataSetProvider;
    LocalConnection1: TLocalConnection;
    dsMovCaixa: TDataSource;
    ActionList1: TActionList;
    acPesquisar: TAction;
    acFecharCaixa: TAction;
    sdsMovCaixaCOD: TIntegerField;
    sdsMovCaixaABERTURA: TDateField;
    sdsMovCaixaFECHAMENTO: TDateField;
    sdsMovCaixaCONTA: TIntegerField;
    sdsMovCaixaSTATUS: TStringField;
    sdsMovCaixaNOME: TStringField;
    sdsMovCaixaCC: TStringField;
    cdsMovCaixaCOD: TIntegerField;
    cdsMovCaixaABERTURA: TDateField;
    cdsMovCaixaFECHAMENTO: TDateField;
    cdsMovCaixaCONTA: TIntegerField;
    cdsMovCaixaSTATUS: TStringField;
    cdsMovCaixaNOME: TStringField;
    cdsMovCaixaCC: TStringField;
    acAbrirCaixa: TAction;
    sdsCaixa: TSQLDataSet;
    dspCaixa: TDataSetProvider;
    cdsCaixa: TClientDataSet;
    sdsCaixaCOD: TIntegerField;
    sdsCaixaABERTURA: TDateField;
    sdsCaixaFECHAMENTO: TDateField;
    sdsCaixaCONTA: TIntegerField;
    sdsCaixaSTATUS: TStringField;
    cdsCaixaCOD: TIntegerField;
    cdsCaixaABERTURA: TDateField;
    cdsCaixaFECHAMENTO: TDateField;
    cdsCaixaCONTA: TIntegerField;
    cdsCaixaSTATUS: TStringField;
    Panel1: TPanel;
    dbgMov: TDBGrid;
    cdsMovimentoCaixa: TClientDataSet;
    cdsMovimentoCaixaCOD: TIntegerField;
    cdsMovimentoCaixaCODACERTO: TIntegerField;
    cdsMovimentoCaixaDINHEIRO: TFloatField;
    cdsMovimentoCaixaCHEQUE: TFloatField;
    cdsMovimentoCaixaCREDITO: TFloatField;
    cdsMovimentoCaixaDEBITO: TFloatField;
    cdsMovimentoCaixaTPMOV: TStringField;
    cdsMovimentoCaixaDATA: TDateField;
    Label3: TLabel;
    lbEntradas: TLabel;
    Label4: TLabel;
    lbSaidas: TLabel;
    Label5: TLabel;
    lbSaldoFinal: TLabel;
    dsMovimentoCaixa: TDataSource;
    btnVoltar: TBitBtn;
    sdsCaixaSALDOFINAL: TFloatField;
    sdsCaixaSALDOINICIAL: TSingleField;
    cdsCaixaSALDOFINAL: TFloatField;
    cdsCaixaSALDOINICIAL: TSingleField;
    sdsMovCaixaSALDOFINAL: TFloatField;
    sdsMovCaixaSALDOINICIAL: TSingleField;
    cdsMovCaixaSALDOFINAL: TFloatField;
    cdsMovCaixaSALDOINICIAL: TSingleField;
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acPesquisarExecute(Sender: TObject);
    procedure acFecharCaixaExecute(Sender: TObject);
    procedure acAbrirCaixaExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dbgCaixaDblClick(Sender: TObject);
    procedure btnVoltarClick(Sender: TObject);
  private
    { Private declarations }
    procedure getCC;
    procedure verificaCaixa;
    procedure abrirCaixa;
    procedure carregaMovCaixa(ACodCx: Integer);
    function getSaldoInicial:Double;
    function getSaldoFinal(ACodCx:Integer):Double;
  public
    { Public declarations }
  end;

var
  FrmMovCaixa: TFrmMovCaixa;

implementation

{$R *.dfm}

uses uDM;

procedure TFrmMovCaixa.abrirCaixa;
begin
  cdsCaixa.Close;
  cdsCaixa.CommandText := 'select * from FIN_CAIXA';
  cdsCaixa.Open;
  cdsCaixa.Append;
  cdsCaixaABERTURA.Value     := Now;
  cdsCaixaCONTA.Value        := cbxCC.ItemIndex;
  cdsCaixaSTATUS.Value       := 'ABE';
  cdsCaixaSALDOINICIAL.Value := getSaldoInicial;
  cdsCaixa.ApplyUpdates(0);
  cdsCaixa.Close;
end;

procedure TFrmMovCaixa.acAbrirCaixaExecute(Sender: TObject);
begin
 if cbxCC.Text = 'TODAS' then
 begin
   ShowMessage('Favor selecionar a conta para abertura');
   Exit;
 end;
 verificaCaixa;
 abrirCaixa;
 btnPesquisar.Click;
end;

procedure TFrmMovCaixa.acFecharCaixaExecute(Sender: TObject);

begin
   if cdsMovCaixa.FieldByName('STATUS').Value = 'FEC' then
 begin
   ShowMessage('N�o � possivel fechar este caixa!');
   exit;
 end;
 cdsCaixa.Close;
 cdsCaixa.CommandText := ' select * from FIN_CAIXA cx where cx.cod = '+ IntToStr(cdsMovCaixaCOD.Value);
 cdsCaixa.Open;
 cdsCaixa.Edit;
 cdsCaixaSTATUS.Value := 'FEC';
 cdsCaixaSALDOFINAL.Value := getSaldoFinal(cdsMovCaixaCOD.Value);
 cdsCaixaFECHAMENTO.Value := Now;
 cdsCaixa.ApplyUpdates(0);
 cdsCaixa.Close;
 btnPesquisar.Click;
end;

procedure TFrmMovCaixa.acPesquisarExecute(Sender: TObject);
var
 S : TStringList;
 datai, dataf: string;
begin
 S := TStringList.Create;
 cdsMovCaixa.Close;
 S.Add('  select cx.*, cc.nome, cc.cc ');
 S.Add('  from fin_caixa cx');
 S.Add('  join fin_contacorrente cc on (cx.conta = cc.cod)');
 S.Add('  where 1=1 ');

 datai := FormatDateTime('dd.mm.yyyy',dtAberturaI.Date);
 dataf := FormatDateTime('dd.mm.yyyy',dtAberturaF.Date);

 S.Add('  and cx.abertura between '+ QuotedStr(datai)+' and '+QuotedStr(dataf));
 if cbxCC.ItemIndex <> 0 then
 begin
   S.Add(' and cx.conta = '+ IntToStr(cbxCC.ItemIndex));
 end;
 S.Add('  order by cc.cod, cx.cod');
 cdsMovCaixa.CommandText := S.Text;
 cdsMovCaixa.Open;
end;

procedure TFrmMovCaixa.BitBtn2Click(Sender: TObject);
begin
  Close;
end;

procedure TFrmMovCaixa.btnVoltarClick(Sender: TObject);
begin
 cdsMovimentoCaixa.Close;
 PageControl1.ActivePage := tbCaixa;
end;

procedure TFrmMovCaixa.carregaMovCaixa(ACodCx: Integer);
var
 S: TStringList;
 debitos,creditos,saldoInicial : Double;
 Qry : TSQLQuery;
begin
 if cdsCaixa.Active then
  cdsCaixa.Close;

 S := TStringList.Create;
 S.Add('select pg.*, a.*, cx.*                          ');
 S.Add('from fin_acerto a                               ');
 S.Add('  join fin_pgto   pg on (a.cod = pg.codacerto)  ');
 S.Add('  join fin_caixa  cx on (cx.cod = a.codcaixa)   ');
 S.Add('where cx.cod = '+ IntToStr(ACodCx)               );
 S.Add('order by pg.tpmov, pg.cod                       ');

 Qry := TSQLQuery.Create(nil);
 Qry.SQLConnection := DM.DBAcesso;
 Qry.CommandText := S.Text;
 Qry.Open;


 cdsMovimentoCaixa.Open;
 cdsMovimentoCaixa.DisableControls;
 while not Qry.Eof do
 begin
  cdsMovimentoCaixa.Append;
  cdsMovimentoCaixaCOD.Value       := Qry.FieldByName('COD').Value;
  cdsMovimentoCaixaCODACERTO.Value := Qry.FieldByName('COD').Value;
  cdsMovimentoCaixaDINHEIRO.Value  := Qry.FieldByName('DINHEIRO').Value;
  cdsMovimentoCaixaCHEQUE.Value    := Qry.FieldByName('CHEQUE').Value;
  cdsMovimentoCaixaCREDITO.Value   := Qry.FieldByName('CREDITO').Value;
  cdsMovimentoCaixaDEBITO.Value    := Qry.FieldByName('DEBITO').Value;
  if Qry.FieldByName('TPOPERACAO').AsString = '1' then
    cdsMovimentoCaixaTPMOV.AsString  := 'S'
  else if Qry.FieldByName('TPOPERACAO').AsString = '2' then
    cdsMovimentoCaixaTPMOV.AsString  := 'E';
  cdsMovimentoCaixaDATA.Value      := Qry.FieldByName('DATA').Value;
  cdsMovimentoCaixa.Post;
  Qry.Next;
 end;
 cdsMovimentoCaixa.EnableControls;

 //Pega o valor total dos Debitos
 S.Clear;
 S.Add('select coalesce(sum( pg.dinheiro + pg.cheque + pg.credito + pg.debito),0) as total  ');
 S.Add('  from fin_acerto a                                                                 ');
 S.Add('  join fin_pgto   pg on (a.cod = pg.codacerto)                                      ');
 S.Add('where pg.tpmov = ''D''                                                              ');
 S.Add('  and a.codcaixa = '+ IntToStr(ACodCx)                                               );

 Qry.Close;
 Qry.CommandText := S.Text;
 Qry.Open;

 debitos := Qry.FieldByName('TOTAL').Value;

 //Pega o valor dos Creditos
 S.Clear;
 S.Add('select coalesce(sum( pg.dinheiro + pg.cheque + pg.credito + pg.debito),0) as total  ');
 S.Add('  from fin_acerto a                                                                 ');
 S.Add('  join fin_pgto   pg on (a.cod = pg.codacerto)                                      ');
 S.Add('where pg.tpmov = ''C''                                                              ');
 S.Add('  and a.codcaixa = '+ IntToStr(ACodCx)                                               );
 Qry.Close;
 Qry.CommandText := S.Text;
 Qry.Open;

 creditos := Qry.FieldByName('TOTAL').Value;
 saldoInicial := cdsMovCaixaSALDOINICIAL.Value;

 lbEntradas.Caption   := FormatFloat('##0.00',creditos);
 lbSaidas.Caption     := FormatFloat('##0.00',debitos);
 lbSaldoFinal.Caption := FormatFloat('##0.00',saldoInicial+(creditos-debitos) );
 FreeAndNil(Qry);
 FreeAndNil(S);
end;

procedure TFrmMovCaixa.dbgCaixaDblClick(Sender: TObject);
begin
 carregaMovCaixa(cdsMovCaixaCOD.Value);
 PageControl1.ActivePage := tbMov;
end;

procedure TFrmMovCaixa.FormCreate(Sender: TObject);
begin
 getCC;
 tbCaixa.TabVisible := FALSE;
 tbMov.TabVisible   := False;
 PageControl1.ActivePage := tbCaixa;
end;

procedure TFrmMovCaixa.FormShow(Sender: TObject);
begin
 dtAberturaI.Date := Now - 7;
 dtAberturaF.Date := Now;
 btnPesquisar.Click;
end;

procedure TFrmMovCaixa.getCC;
begin
 with dm.QryTemp,SQL do
 begin
   Clear;
   Close;
   Add('select * from fin_contacorrente cc');
   Add('order by cc.cod');
   Open;
 end;


 cbxCC.Clear;
 cbxCC.Items.Add('TODAS');
 DM.QryTemp.First;
 while not DM.QryTemp.Eof do
 begin
   cbxcc.Items.Add(DM.QryTemp.FieldByName('NOME').AsString);
   dm.QryTemp.Next;
 end;
 cbxCC.ItemIndex := 0;

end;

function TFrmMovCaixa.getSaldoFinal(ACodCx:Integer): Double;
var
 Qry : TSQLQuery;
 S : TStringList;
 debitos, creditos,saldoinicial: Double;
begin
 try

  Qry := TSQLQuery.Create(nil);
  Qry.SQLConnection := DM.DBAcesso;
  S   := TStringList.Create;

  //Pega o valor total dos Debitos
  S.Clear;
  S.Add('select coalesce(sum( pg.dinheiro + pg.cheque + pg.credito + pg.debito),0) as total  ');
  S.Add('  from fin_acerto a                                                                 ');
  S.Add('  join fin_pgto   pg on (a.cod = pg.codacerto)                                      ');
  S.Add('where pg.tpmov = ''D''                                                              ');
  S.Add('  and a.codcaixa = '+ IntToStr(ACodCx)                                               );

  Qry.Close;
  Qry.CommandText := S.Text;
  Qry.Open;

  debitos := Qry.FieldByName('TOTAL').Value;

  //Pega o valor dos Creditos
  S.Clear;
  S.Add('select coalesce(sum( pg.dinheiro + pg.cheque + pg.credito + pg.debito),0) as total  ');
  S.Add('  from fin_acerto a                                                                 ');
  S.Add('  join fin_pgto   pg on (a.cod = pg.codacerto)                                      ');
  S.Add('where pg.tpmov = ''C''                                                              ');
  S.Add('  and a.codcaixa = '+ IntToStr(ACodCx)                                               );
  Qry.Close;
  Qry.CommandText := S.Text;
  Qry.Open;

  creditos := Qry.FieldByName('TOTAL').Value;
  saldoinicial := cdsMovCaixaSALDOINICIAL.Value;
  Result := saldoinicial+(creditos - debitos);

 finally
  FreeAndNil(S);
  FreeAndNil(Qry);
 end;

end;

function TFrmMovCaixa.getSaldoInicial: Double;
begin
 with DM.QryTemp,SQL do
 begin
   Clear;
   Close;
   Add(' select pr.saldo from  fin_saldo_caixa ('+ IntToStr(cbxCC.ItemIndex) +') pr');
   Open;
 end;
 Result := DM.QryTemp.FieldByName('SALDO').AsFloat;
end;

procedure TFrmMovCaixa.verificaCaixa;
begin
 with DM.QryTemp,SQL do
 begin
  Clear;
  Close;
  Add(' select cx.status');
  Add(' from fin_caixa cx');
  Add(' join fin_contacorrente cc on (cc.cod = cx.conta)');
  Add(' where cx.status = ''ABE'' ');
  Add(' and cc.cod = '+ IntToStr(cbxCC.ItemIndex));
  Open;
 end;
 if not DM.QryTemp.IsEmpty then
 begin
   ShowMessage('Esta conta j� possui um caixa aberto');
   Abort;
 end;
end;

end.
