unit uFrmProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFrmModelo, Data.DB, Vcl.ImgList,
  Vcl.ActnList, Vcl.Menus, Vcl.ComCtrls, Vcl.ToolWin, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Mask, Vcl.DBCtrls, uDM,
  System.Actions, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  EhLibVCL, GridsEh, DBAxisGridsEh, DBGridEh;

type
  TFrmProduto = class(TFrmModelo)
    Label4: TLabel;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    Label7: TLabel;
    edtReferencia: TDBEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    DBLookupComboBox2: TDBLookupComboBox;
    Label8: TLabel;
    Label9: TLabel;
    cbStatusProduto: TDBCheckBox;
    procedure ac_IncluirExecute(Sender: TObject);
    procedure ac_PesquisarExecute(Sender: TObject);
    procedure ac_SalvarExecute(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmProduto: TFrmProduto;
  FIncluindo: Boolean;

implementation

uses
  Data.SqlExpr, uRotinas;

{$R *.dfm}

procedure TFrmProduto.ac_IncluirExecute(Sender: TObject);
VAR
 QRYGEN: TSQLQuery;
begin
  if FIncluindo then
  begin
    if edtReferencia.Text = EmptyStr then
    begin
      ShowMessage('N�o pode incluir o produto sem Referencia!');
      edtReferencia.SetFocus;
      Exit;
    end;

    if DBEdit1.Text = EmptyStr then
    begin
      ShowMessage('N�o pode incluir o produto sem Descri��o!');
      DBEdit1.SetFocus;
      Exit;
    end;

    if (DBLookupComboBox1.Text = EmptyStr) or (DBLookupComboBox2.Text = EmptyStr) then
    begin
      ShowMessage('N�o pode incluir o produto sem Fabricante ou Grupo!');
      Exit;
    end;

    if DBEdit3.Text = EmptyStr then
    begin
      ShowMessage('N�o pode incluir o produto sem Pre�o!');
      Exit;
    end;



  end;
  inherited;
  if not FIncluindo then
  begin
    QRYGEN := TSQLQuery.Create(SELF);
    QRYGEN.SQLConnection := dm.DBAcesso;
    QRYGEN.SQL.Add('Select GEN_ID(GEN_PRODUTO_REFERENCIA,1) FROM RDB$DATABASE');
    QRYGEN.Open;
    edtReferencia.Text := QRYGEN.FieldByName('GEN_ID').AsString;
    dm.cdsProdutosESTOQUE.Value := 0;
    FIncluindo := True;
  end;
  DBEdit1.SetFocus;
end;

procedure TFrmProduto.ac_PesquisarExecute(Sender: TObject);
begin
 if RadioGroup1.ItemIndex = 0 then
 begin
   DM.sdsProduto.CommandText := 'select * from PRODUTOS where STATUS = ''A'' ';
 end
 else
 begin
   DM.sdsProduto.CommandText := 'select * from PRODUTOS where STATUS = ''I'' ';
 end;
  inherited;

end;

procedure TFrmProduto.ac_SalvarExecute(Sender: TObject);
begin
  GravarLog(DM.cdsProdutosPRODUTOID.Value,'CAD PRODUTO');
  inherited;

end;

procedure TFrmProduto.FormCreate(Sender: TObject);
begin
  inherited;
  FIncluindo := False;
end;

procedure TFrmProduto.PageControl1Change(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePage = tbCadastro then
    DBEdit1.SetFocus;
end;

end.
