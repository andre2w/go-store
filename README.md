![GO! Store-logo.png](https://bitbucket.org/repo/AERzAG/images/1639573987-GO!%20Store-logo.png)

# Go! Store v1.0 


### Sobre o sistema ###

O Go Store é um sistema básico de vendas. Nele é possível encontrar:

 * Cadastro de Pessoas
 * Cadastro de Produtos
 * PDV
 * Pedido Consignado
 * Contas a Pagar e Receber
 * Controle de Caixa
 * Controle de Estoque

### Compilando ###

Para compilar é preciso do Delphi XE2+ e dos seguintes componentes:

* Edit2w(https://github.com/andre2w/edit2w)
* EhLib
 
### Database ###
Para utilizar o banco de dados é necessário o firebird 2.5. 

 * Criar banco de dados vazio.
 * Executar o GoStore Schemas.sql
 * Executar usuarioAdm.sql para criar Perfil e usuário administrador.
 * Dentro do arquivo dbxconnections.ini adicionar o caminho do banco de dados no campo DATABASE. Este arquivo deve ficar junto com o EXE. 


### Outras informações ###
Os ícones encontrados no sistemas são compartilhados por Creative Commoms através do [The Noun Project](https://thenounproject.com/)