unit uRotinas;

interface

procedure GravarLog (ATransacao:Integer;AModulo:string);
function GetCodGenerator (AGenerator:string):Integer;
function GetDataFormatada (AData:TDate):string;
function GetCodCaixa(ACodCC:Integer):Integer;
function VerificaCaixaAberto(ACodCC:Integer):Boolean;

implementation

uses uDM, System.SysUtils, uUsuario;

procedure GravarLog (ATransacao:Integer;AModulo:string);
begin
 with DM do
 begin
   cdsUsu_log.Open;
   cdsUsu_log.Append;
   cdsUsu_logTRANSACAO.Value      := ATransacao;
   cdsUsu_logDATAHORA.AsDateTime  := Now ;
   cdsUsu_logUSUARIO.Value        := TUsuario.GetInstance.ID;
   cdsUsu_logMODULO.Value         := AModulo;
   cdsUsu_log.Post;
   cdsUsu_log.ApplyUpdates(0);
   cdsUsu_log.Close;
 end;
end;

function GetCodGenerator (AGenerator:string):Integer;
begin
 try
  with DM.QryTemp,SQL do
  begin
   Clear;
   Close;
   Add('Select GEN_ID('+AGenerator+',1) FROM RDB$DATABASE');
   Open;
  end;
  Result := DM.QryTemp.FieldByName('GEN_ID').AsInteger;
 finally
  DM.QryTemp.Close;
 end;

end;

function GetDataFormatada (AData:TDate):string;
begin
 Result := FormatDateTime('dd.mm.yyyy', AData);
end;

function GetCodCaixa(ACodCC:Integer):Integer;
begin
  try
    with DM.QryTemp,SQL do
    begin
      Clear;
      Close;
      Add('select cx.cod from fin_caixa cx');
      Add('where cx.conta = '+ IntToStr(ACodCC)+' and cx.status = ''ABE'' ');
      Open;
    end;
    Result := DM.QryTemp.FieldByName('cod').Value;
  finally
   DM.QryTemp.Close;
  end;
end;

//Se retornar True - O caixa esta aberto.
function VerificaCaixaAberto(ACodCC:Integer):Boolean;
begin
 with DM.QryTemp,SQL do
 begin
  Clear;
  Close;
  Add(' select cx.status');
  Add(' from fin_caixa cx');
  Add(' join fin_contacorrente cc on (cc.cod = cx.conta)');
  Add(' where cx.status = ''ABE'' ');
  Add(' and cc.cod = '+ IntToStr(ACodCC));
  Open;
 end;

 if not DM.QryTemp.IsEmpty then
   Result := True
 else
   Result := False;
end;

end.
